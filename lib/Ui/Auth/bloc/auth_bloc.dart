import 'package:bloc/bloc.dart';
import 'package:todotest/Ui/Auth/bloc/auth_state.dart';
import 'package:todotest/Ui/Auth/bloc/auth_event.dart';

import 'package:todotest/data/repository/irepository.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  IRepository _repository;

  AuthBloc(this._repository) : super(AuthState.initail());
  @override
  AuthState get initialState => AuthState.initail();


  @override
  Stream<AuthState> mapEventToState(
      AuthEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }

    if (event is ChangeStatus) {
      yield state.rebuild((b) => b..success = false..user = null
        );
    }


    if (event is Login) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..user=null
          ..success=false
        );

        final user = await _repository.login(
          event.email,
          event.password,
        );
        //await _repository.refreshToken(user.refreshToken);
        // final isSuccess = await _repository.addfcmToken(
        //   event.fcmToken,
        // );

        print('Login Success data ${user}');
       // print('addfcmToken Success data ${isSuccess}');

        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..success=true

        );
      } catch (e) {
        print('Login Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = e.error.toString()
          ..success=false


        );
      }
    }

  }
}
