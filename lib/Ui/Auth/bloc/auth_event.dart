library auth_event;

import 'dart:convert';
import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'auth_event.g.dart';

abstract class AuthEvent {}

abstract class ClearError extends AuthEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}


abstract class SignUp extends AuthEvent
    implements Built<SignUp, SignUpBuilder> {
  // fields go here

  String get name;
  String get email;
  String get phoneNumber;
  String get password;
  String get passwordConfirmation;
  File get avatar;
  String get location;
  double get longitude;
  double get latitude;

  SignUp._();

  factory SignUp([updates(SignUpBuilder b)]) = _$SignUp;
}
abstract class Verify extends AuthEvent
    implements Built<Verify, VerifyBuilder> {
  // fields go here

  String get email;
  int get activationCode;
  String get fcmToken;

  Verify._();

  factory Verify([updates(VerifyBuilder b)]) = _$Verify;
}
abstract class ForgetPassword extends AuthEvent
    implements Built<ForgetPassword, ForgetPasswordBuilder> {
  // fields go here

  String get email;

  ForgetPassword._();

  factory ForgetPassword([updates(ForgetPasswordBuilder b)]) = _$ForgetPassword;
}
abstract class Login extends AuthEvent
    implements Built<Login, LoginBuilder> {
  // fields go here

  String get email;
  String get password;

  Login._();

  factory Login([updates(LoginBuilder b)]) = _$Login;
}
abstract class ChangeStatus extends AuthEvent
    implements Built<ChangeStatus, ChangeStatusBuilder> {
  // fields go here

  ChangeStatus._();

  factory ChangeStatus([updates(ChangeStatusBuilder b)]) = _$ChangeStatus;
}








