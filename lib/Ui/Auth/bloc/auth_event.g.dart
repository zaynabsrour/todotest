// GENERATED CODE - DO NOT MODIFY BY HAND

part of auth_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$SignUp extends SignUp {
  @override
  final String name;
  @override
  final String email;
  @override
  final String phoneNumber;
  @override
  final String password;
  @override
  final String passwordConfirmation;
  @override
  final File avatar;
  @override
  final String location;
  @override
  final double longitude;
  @override
  final double latitude;

  factory _$SignUp([void Function(SignUpBuilder) updates]) =>
      (new SignUpBuilder()..update(updates)).build();

  _$SignUp._(
      {this.name,
      this.email,
      this.phoneNumber,
      this.password,
      this.passwordConfirmation,
      this.avatar,
      this.location,
      this.longitude,
      this.latitude})
      : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('SignUp', 'name');
    }
    if (email == null) {
      throw new BuiltValueNullFieldError('SignUp', 'email');
    }
    if (phoneNumber == null) {
      throw new BuiltValueNullFieldError('SignUp', 'phoneNumber');
    }
    if (password == null) {
      throw new BuiltValueNullFieldError('SignUp', 'password');
    }
    if (passwordConfirmation == null) {
      throw new BuiltValueNullFieldError('SignUp', 'passwordConfirmation');
    }
    if (avatar == null) {
      throw new BuiltValueNullFieldError('SignUp', 'avatar');
    }
    if (location == null) {
      throw new BuiltValueNullFieldError('SignUp', 'location');
    }
    if (longitude == null) {
      throw new BuiltValueNullFieldError('SignUp', 'longitude');
    }
    if (latitude == null) {
      throw new BuiltValueNullFieldError('SignUp', 'latitude');
    }
  }

  @override
  SignUp rebuild(void Function(SignUpBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SignUpBuilder toBuilder() => new SignUpBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SignUp &&
        name == other.name &&
        email == other.email &&
        phoneNumber == other.phoneNumber &&
        password == other.password &&
        passwordConfirmation == other.passwordConfirmation &&
        avatar == other.avatar &&
        location == other.location &&
        longitude == other.longitude &&
        latitude == other.latitude;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, name.hashCode), email.hashCode),
                                phoneNumber.hashCode),
                            password.hashCode),
                        passwordConfirmation.hashCode),
                    avatar.hashCode),
                location.hashCode),
            longitude.hashCode),
        latitude.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SignUp')
          ..add('name', name)
          ..add('email', email)
          ..add('phoneNumber', phoneNumber)
          ..add('password', password)
          ..add('passwordConfirmation', passwordConfirmation)
          ..add('avatar', avatar)
          ..add('location', location)
          ..add('longitude', longitude)
          ..add('latitude', latitude))
        .toString();
  }
}

class SignUpBuilder implements Builder<SignUp, SignUpBuilder> {
  _$SignUp _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _phoneNumber;
  String get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String phoneNumber) => _$this._phoneNumber = phoneNumber;

  String _password;
  String get password => _$this._password;
  set password(String password) => _$this._password = password;

  String _passwordConfirmation;
  String get passwordConfirmation => _$this._passwordConfirmation;
  set passwordConfirmation(String passwordConfirmation) =>
      _$this._passwordConfirmation = passwordConfirmation;

  File _avatar;
  File get avatar => _$this._avatar;
  set avatar(File avatar) => _$this._avatar = avatar;

  String _location;
  String get location => _$this._location;
  set location(String location) => _$this._location = location;

  double _longitude;
  double get longitude => _$this._longitude;
  set longitude(double longitude) => _$this._longitude = longitude;

  double _latitude;
  double get latitude => _$this._latitude;
  set latitude(double latitude) => _$this._latitude = latitude;

  SignUpBuilder();

  SignUpBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _email = _$v.email;
      _phoneNumber = _$v.phoneNumber;
      _password = _$v.password;
      _passwordConfirmation = _$v.passwordConfirmation;
      _avatar = _$v.avatar;
      _location = _$v.location;
      _longitude = _$v.longitude;
      _latitude = _$v.latitude;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SignUp other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SignUp;
  }

  @override
  void update(void Function(SignUpBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SignUp build() {
    final _$result = _$v ??
        new _$SignUp._(
            name: name,
            email: email,
            phoneNumber: phoneNumber,
            password: password,
            passwordConfirmation: passwordConfirmation,
            avatar: avatar,
            location: location,
            longitude: longitude,
            latitude: latitude);
    replace(_$result);
    return _$result;
  }
}

class _$Verify extends Verify {
  @override
  final String email;
  @override
  final int activationCode;
  @override
  final String fcmToken;

  factory _$Verify([void Function(VerifyBuilder) updates]) =>
      (new VerifyBuilder()..update(updates)).build();

  _$Verify._({this.email, this.activationCode, this.fcmToken}) : super._() {
    if (email == null) {
      throw new BuiltValueNullFieldError('Verify', 'email');
    }
    if (activationCode == null) {
      throw new BuiltValueNullFieldError('Verify', 'activationCode');
    }
    if (fcmToken == null) {
      throw new BuiltValueNullFieldError('Verify', 'fcmToken');
    }
  }

  @override
  Verify rebuild(void Function(VerifyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  VerifyBuilder toBuilder() => new VerifyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Verify &&
        email == other.email &&
        activationCode == other.activationCode &&
        fcmToken == other.fcmToken;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, email.hashCode), activationCode.hashCode),
        fcmToken.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Verify')
          ..add('email', email)
          ..add('activationCode', activationCode)
          ..add('fcmToken', fcmToken))
        .toString();
  }
}

class VerifyBuilder implements Builder<Verify, VerifyBuilder> {
  _$Verify _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  int _activationCode;
  int get activationCode => _$this._activationCode;
  set activationCode(int activationCode) =>
      _$this._activationCode = activationCode;

  String _fcmToken;
  String get fcmToken => _$this._fcmToken;
  set fcmToken(String fcmToken) => _$this._fcmToken = fcmToken;

  VerifyBuilder();

  VerifyBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _activationCode = _$v.activationCode;
      _fcmToken = _$v.fcmToken;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Verify other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Verify;
  }

  @override
  void update(void Function(VerifyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Verify build() {
    final _$result = _$v ??
        new _$Verify._(
            email: email, activationCode: activationCode, fcmToken: fcmToken);
    replace(_$result);
    return _$result;
  }
}

class _$ForgetPassword extends ForgetPassword {
  @override
  final String email;

  factory _$ForgetPassword([void Function(ForgetPasswordBuilder) updates]) =>
      (new ForgetPasswordBuilder()..update(updates)).build();

  _$ForgetPassword._({this.email}) : super._() {
    if (email == null) {
      throw new BuiltValueNullFieldError('ForgetPassword', 'email');
    }
  }

  @override
  ForgetPassword rebuild(void Function(ForgetPasswordBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ForgetPasswordBuilder toBuilder() =>
      new ForgetPasswordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ForgetPassword && email == other.email;
  }

  @override
  int get hashCode {
    return $jf($jc(0, email.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ForgetPassword')..add('email', email))
        .toString();
  }
}

class ForgetPasswordBuilder
    implements Builder<ForgetPassword, ForgetPasswordBuilder> {
  _$ForgetPassword _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  ForgetPasswordBuilder();

  ForgetPasswordBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ForgetPassword other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ForgetPassword;
  }

  @override
  void update(void Function(ForgetPasswordBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ForgetPassword build() {
    final _$result = _$v ?? new _$ForgetPassword._(email: email);
    replace(_$result);
    return _$result;
  }
}

class _$Login extends Login {
  @override
  final String email;
  @override
  final String password;

  factory _$Login([void Function(LoginBuilder) updates]) =>
      (new LoginBuilder()..update(updates)).build();

  _$Login._({this.email, this.password}) : super._() {
    if (email == null) {
      throw new BuiltValueNullFieldError('Login', 'email');
    }
    if (password == null) {
      throw new BuiltValueNullFieldError('Login', 'password');
    }
  }

  @override
  Login rebuild(void Function(LoginBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoginBuilder toBuilder() => new LoginBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Login && email == other.email && password == other.password;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, email.hashCode), password.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Login')
          ..add('email', email)
          ..add('password', password))
        .toString();
  }
}

class LoginBuilder implements Builder<Login, LoginBuilder> {
  _$Login _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _password;
  String get password => _$this._password;
  set password(String password) => _$this._password = password;

  LoginBuilder();

  LoginBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _password = _$v.password;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Login other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Login;
  }

  @override
  void update(void Function(LoginBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Login build() {
    final _$result = _$v ?? new _$Login._(email: email, password: password);
    replace(_$result);
    return _$result;
  }
}

class _$ChangeStatus extends ChangeStatus {
  factory _$ChangeStatus([void Function(ChangeStatusBuilder) updates]) =>
      (new ChangeStatusBuilder()..update(updates)).build();

  _$ChangeStatus._() : super._();

  @override
  ChangeStatus rebuild(void Function(ChangeStatusBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChangeStatusBuilder toBuilder() => new ChangeStatusBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChangeStatus;
  }

  @override
  int get hashCode {
    return 20091075;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ChangeStatus').toString();
  }
}

class ChangeStatusBuilder
    implements Builder<ChangeStatus, ChangeStatusBuilder> {
  _$ChangeStatus _$v;

  ChangeStatusBuilder();

  @override
  void replace(ChangeStatus other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ChangeStatus;
  }

  @override
  void update(void Function(ChangeStatusBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChangeStatus build() {
    final _$result = _$v ?? new _$ChangeStatus._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
