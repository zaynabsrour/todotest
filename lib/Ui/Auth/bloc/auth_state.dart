library auth_state;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:todotest/model/user/user.dart';
import 'package:todotest/model/user_response/user_response.dart';


part 'auth_state.g.dart';

abstract class AuthState
    implements Built<AuthState, AuthStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;

 @nullable
  User get user;
  bool get success;
  //bool get successfb;
  bool get isLogin;

  AuthState._();

  factory AuthState([updates(AuthStateBuilder b)]) =
  _$AuthState;

  factory AuthState.initail() {
    return AuthState((b) => b
      ..error = ""
      ..isLoading = false
      ..success = false
      ..isLogin = false
      ..user=null);
  }
}
