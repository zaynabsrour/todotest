import 'package:flutter/material.dart';
 import 'package:todotest/Ui/mainPage/mainPage.dart';
 import 'package:todotest/core/constent.dart';
  import 'package:todotest/Ui/Auth/bloc/auth_bloc.dart';
import 'package:todotest/Ui/Auth/bloc/auth_event.dart';
import 'package:todotest/Ui/Auth/bloc/auth_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:todotest/core/style/base_color.dart';
import 'package:todotest/core/style/custom_loader.dart';
import 'package:todotest/app/App.dart' ;
class ClientLoginPage extends StatefulWidget {
  final AuthBloc bloc;
  ClientLoginPage(this.bloc);
  @override
  _ClientLoginPageState createState() => _ClientLoginPageState();
}

class _ClientLoginPageState extends State<ClientLoginPage> {

  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
  TextEditingController email;
  TextEditingController password;
  @override
  void initState() {
  email = TextEditingController();
   password = TextEditingController();  
     super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColor.background2,
      body: BlocBuilder(
        cubit: widget.bloc,
        builder: (context,AuthState state) {
          error(state.error);
          if (state.success)
          {  WidgetsBinding.instance.addPostFrameCallback((_) =>
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) =>
                        MainPage())));
          widget.bloc.add(ChangeStatus());
        }
        return state.isLoading?
  Center(child: loader(context: context),)
  :
        SingleChildScrollView(
                  child: Container(
                    height: size.height,
                    width: size.width,
                    child: Stack(
            alignment: Alignment.center,
            
            children: <Widget>[
              // background image and bottom contents
              Column(
                children: <Widget>[
                    Stack(
                      children: [
                        Container(
                          height: 170.0,
                        ),
                        Positioned(
                          top: 50,
                          left: 10,
                          child: IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 30,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                        Positioned(
                          top: 120,
                          left: 20,
                          child: Text(

                              'login',

                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 25),
                          ),
                        ),
                        Positioned(
                          top: -95,
                          right: -75,
                          child: Image.asset('assets/image/top_l_corner.png'),
                        ),
                      ],
                    ),
                    Expanded(
                      child: Container(
                          width: size.width,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(15),
                                topLeft: Radius.circular(15)),
                            color: Colors.white,
                          ),
                          ),
                    )
                ],
              ),
              // Profile image
              Positioned(
                    top: 90,  child: Column(
                      
                                    crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset('assets/image/logo.png',fit: BoxFit.fill,),
                      Container(
                          // height: size.height*0.75,
                          width: size.width,
                          
                            padding: EdgeInsets.only(
                                // top: 26.0,
                                left: size.width * 0.1,
                                right: size.width * 0.1),
                             
                          child: Column(
                            children: [
                              // Image.asset('assets/image/logo.png'),
                              // SizedBox(
                              //   height: size.height * 0.1,
                              // ),
                              Row(
                                children: [
                                  Text(

                                      'WelcomeBack',

                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        color: Colors.black,
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(

                                      'loginInformation',

                                    style: TextStyle(
                                        color: AppColor.greyColor, fontSize: 12),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Row(
                                children: [
                                  Text(

                                      'Email',

                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        color: Colors.black,
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: TextFormField(
                                  controller: email,
                                    textAlign: TextAlign.start,
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        fieldDecoration('example@example.com')
                                            .copyWith(
                                                suffixIcon: Padding(
                                                  padding:
                                                      const EdgeInsets.all(16.0),
                                                  child: Icon(
                                                    Icons.check_box,
                                                    color: AppColor.yallowDark,
                                                  ),
                                                ),
                                                prefixIcon: Image.asset(
                                                  'assets/image/mail.png',
                                                  width: 24,
                                                  height: 24,
                                                ))),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Row(
                                children: [
                                  Text(

                                      'password',

                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        color: Colors.black,
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: TextFormField(
                                  controller: password,
                                    obscureText: _obscureText,
                                    textAlign: TextAlign.start,
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        fieldDecoration('***********').copyWith(
                                            suffixIcon: GestureDetector(
                                                onTap: () {
                                                  _toggle();
                                                },
                                                child: Image.asset(
                                                  'assets/image/eye.png',
                                                  width: 24,
                                                  height: 24,
                                                )),
                                            prefixIcon: Image.asset(
                                              'assets/image/lock.png',
                                              width: 24,
                                              height: 24,
                                            ))),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              InkWell(
                                onTap: () {

                                },
                                child: Row(
                                  children: [
                                    Text(

                                        'forgetPassword?',

                                      style: TextStyle(
                                          fontWeight: FontWeight.normal,
                                          color: AppColor.textColor,
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              InkWell(
                                onTap: () {
                                  widget.bloc.add(Login((b)=>b
                                  ..email=email.text
                                      ..password=password.text
                                  ));


                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: Container(
                                          width: size.width * 0.8,
                                          decoration: BoxDecoration(
                                              color: AppColor.buttonColor,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(16))),
                                          child: Center(
                                              child: Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: Text(


                                                'login',

                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ))),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: size.height * 0.05,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 18.0, top: 10, bottom: 18),
                                child: InkWell(
                                  onTap: () {

                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                           'DontHaveAccount',

                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 13),
                                      ),
                                      Text(

                                          'SignUp',

                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: AppColor.textColor,
                                            fontSize: 13),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
           
                      ],
                    )),
            // Positioned(
            //   top:size.height*0.5,
            //           child:  ),
                         
            ],
          ),
                  ),
        );
     
        },
               ),
    );
  }

  InputDecoration fieldDecoration(String hint) {
    return InputDecoration(
      hintText: hint,
      hintStyle: TextStyle(fontSize: 16, color: AppColor.textGreyColor),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(
          width: 0,
          style: BorderStyle.none,
        ),
      ),
      filled: true,
      contentPadding: EdgeInsets.all(16),
      fillColor: AppColor.fillGreyColor,
    );
  }
  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          // timeInSecForIosWeb: 1,
          backgroundColor: containerColor,
          textColor: Colors.white,
          fontSize: 16.0);
      widget.bloc.add(ClearError());
    }
  }
}
