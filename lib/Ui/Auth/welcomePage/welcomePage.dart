import 'package:flutter/material.dart';
import 'package:todotest/Ui/Auth/clientLoginPage/clientLoginPage.dart';
 import 'package:todotest/Ui/mainPage/mainPage.dart';
 import 'package:todotest/injectoin.dart';
import 'package:todotest/Ui/Auth/bloc/auth_bloc.dart';

import 'package:todotest/core/constent.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
    final _bloc = sl<AuthBloc>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColor.background2,
      body:
      SingleChildScrollView(child:
      Stack (
        children: [
          Column(
            children: [
              SizedBox(
                height: size.height * 0.1,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                        size: 30,
                      ),
                    )
                  ],
                ),
              ),
              Container(
                width: size.width * 0.8,
                height: size.height * 0.335,
                child: Image.asset('assets/image/welcome.png'),
              ),
              SizedBox(
                height: size.height * 0.05,
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        right: 16, left: 16, top: 8, bottom: 8),
                    child: Row(
                      children: [
                        Text(

                            "Welcome",

                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        bottom: 8, right: 16, left: 16, top: 16),
                    child: Row(
                      children: [
                        Text(

                            "ManageExperiences",

                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(right: 16, left: 16, bottom: 8),
                    child: Row(
                      children: [
                        Text(

                            "Seamlessly & Intuitively",

                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => ClientLoginPage(_bloc)));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16),
                  child: Container(
//                  width: size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        color: Colors.white,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: Text(

                                'login',

                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      )),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              InkWell(
                onTap: () {

                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16),
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        border: Border.all(color: Colors.white, width: 1),
                        color: Colors.transparent,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: Text(

                                'SignUp',

                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      )),
                ),
              ),
              SizedBox(height: 16,),
              Divider(color: Colors.white,),
              SizedBox(height: 16,),


            ],
          ),
          Positioned(
            bottom: -85,
            right: -75,
            child: Image.asset('assets/image/btm_l_corner.png'),
          ),
          Positioned(
            top: -75,
            right: -75,
            child: Image.asset('assets/image/top_l_corner.png'),
          ),
        ],
      )),
    );
  }
}
