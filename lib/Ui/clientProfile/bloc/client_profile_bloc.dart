import 'package:bloc/bloc.dart';
import 'package:todotest/Ui/clientProfile/bloc/client_profile_state.dart';
import 'package:todotest/Ui/clientProfile/bloc/client_profile_event.dart';

import 'package:todotest/data/repository/irepository.dart';

class ClientProfileBloc extends Bloc<ClientProfileEvent, ClientProfileState> {
  IRepository _repository;

  ClientProfileBloc(this._repository) : super(ClientProfileState.initail());
  @override
  ClientProfileState get initialState => ClientProfileState.initail();


  @override
  Stream<ClientProfileState> mapEventToState(
      ClientProfileEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is ChangeStatus) {
      yield state.rebuild((b) => b..success = false
        );
    }
    if (event is GetIsLogin) {
      try {
        final result = await _repository.getIsLogin();
        yield state.rebuild((b) => b..isLogin = result);

      } catch (e) {
        print(' Error $e');
        yield state.rebuild((b) => b
          ..error = ""
        )
        ;
      }
    }


    if (event is GetUserProfile) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..success=false
          ..user = null
        );

        final user = await _repository.getUserProfile();


        print('Get User Profile Success data ${user}');

        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..success=true
          ..user.replace(user)
        );
      } catch (e) {
        print('Get User Profile Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = e.error.toString()
          ..success=false

          ..user.replace(null)
        );
      }
    }

  }
  
}
