library client_profile_event;

import 'dart:convert';
import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'client_profile_event.g.dart';
abstract class ClientProfileEvent {}

abstract class ClearError extends ClientProfileEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}

abstract class ChangeStatus extends ClientProfileEvent
    implements Built<ChangeStatus, ChangeStatusBuilder> {
  // fields go here

  ChangeStatus._();

  factory ChangeStatus([updates(ChangeStatusBuilder b)]) = _$ChangeStatus;
}



abstract class GetUserProfile extends ClientProfileEvent
    implements Built<GetUserProfile, GetUserProfileBuilder> {


  GetUserProfile._();

  factory GetUserProfile([updates(GetUserProfileBuilder b)]) = _$GetUserProfile;
}

abstract class GetIsLogin extends ClientProfileEvent implements Built<GetIsLogin,GetIsLoginBuilder> {
  // fields go here



  GetIsLogin._();

  factory GetIsLogin([updates(GetIsLoginBuilder b)]) = _$GetIsLogin;
}



