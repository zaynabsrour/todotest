library client_profile_state;

import 'dart:convert';
import 'package:todotest/model/user/user.dart';
import 'package:built_value/built_value.dart';
import 'package:built_collection/built_collection.dart';
  part 'client_profile_state.g.dart';

abstract class ClientProfileState
    implements Built<ClientProfileState, ClientProfileStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;
  bool get isLogin;


  @nullable
  bool get successNotification;

  bool get success;
  //bool get successfb;

 @nullable
  bool get successReview;
 @nullable
 User get user;

  ClientProfileState._();

  factory ClientProfileState([updates(ClientProfileStateBuilder b)]) =
  _$ClientProfileState;

  factory ClientProfileState.initail() {
    return ClientProfileState((b) => b
      ..error = ""
      ..isLoading = false
      ..isLogin = false
      ..success = false
      ..user=null
      ..successNotification=null
      ..successReview = false
      );
  }
}
