// GENERATED CODE - DO NOT MODIFY BY HAND

part of client_profile_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClientProfileState extends ClientProfileState {
  @override
  final String error;
  @override
  final bool isLoading;
  @override
  final bool isLogin;
  @override
  final bool successNotification;
  @override
  final bool success;
  @override
  final bool successReview;
  @override
  final User user;

  factory _$ClientProfileState(
          [void Function(ClientProfileStateBuilder) updates]) =>
      (new ClientProfileStateBuilder()..update(updates)).build();

  _$ClientProfileState._(
      {this.error,
      this.isLoading,
      this.isLogin,
      this.successNotification,
      this.success,
      this.successReview,
      this.user})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('ClientProfileState', 'error');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('ClientProfileState', 'isLoading');
    }
    if (isLogin == null) {
      throw new BuiltValueNullFieldError('ClientProfileState', 'isLogin');
    }
    if (success == null) {
      throw new BuiltValueNullFieldError('ClientProfileState', 'success');
    }
  }

  @override
  ClientProfileState rebuild(
          void Function(ClientProfileStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClientProfileStateBuilder toBuilder() =>
      new ClientProfileStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClientProfileState &&
        error == other.error &&
        isLoading == other.isLoading &&
        isLogin == other.isLogin &&
        successNotification == other.successNotification &&
        success == other.success &&
        successReview == other.successReview &&
        user == other.user;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, error.hashCode), isLoading.hashCode),
                        isLogin.hashCode),
                    successNotification.hashCode),
                success.hashCode),
            successReview.hashCode),
        user.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ClientProfileState')
          ..add('error', error)
          ..add('isLoading', isLoading)
          ..add('isLogin', isLogin)
          ..add('successNotification', successNotification)
          ..add('success', success)
          ..add('successReview', successReview)
          ..add('user', user))
        .toString();
  }
}

class ClientProfileStateBuilder
    implements Builder<ClientProfileState, ClientProfileStateBuilder> {
  _$ClientProfileState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _isLogin;
  bool get isLogin => _$this._isLogin;
  set isLogin(bool isLogin) => _$this._isLogin = isLogin;

  bool _successNotification;
  bool get successNotification => _$this._successNotification;
  set successNotification(bool successNotification) =>
      _$this._successNotification = successNotification;

  bool _success;
  bool get success => _$this._success;
  set success(bool success) => _$this._success = success;

  bool _successReview;
  bool get successReview => _$this._successReview;
  set successReview(bool successReview) =>
      _$this._successReview = successReview;

  UserBuilder _user;
  UserBuilder get user => _$this._user ??= new UserBuilder();
  set user(UserBuilder user) => _$this._user = user;

  ClientProfileStateBuilder();

  ClientProfileStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _isLoading = _$v.isLoading;
      _isLogin = _$v.isLogin;
      _successNotification = _$v.successNotification;
      _success = _$v.success;
      _successReview = _$v.successReview;
      _user = _$v.user?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ClientProfileState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClientProfileState;
  }

  @override
  void update(void Function(ClientProfileStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClientProfileState build() {
    _$ClientProfileState _$result;
    try {
      _$result = _$v ??
          new _$ClientProfileState._(
              error: error,
              isLoading: isLoading,
              isLogin: isLogin,
              successNotification: successNotification,
              success: success,
              successReview: successReview,
              user: _user?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        _user?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ClientProfileState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
