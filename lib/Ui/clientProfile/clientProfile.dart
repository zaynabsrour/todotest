import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
  import 'package:todotest/injectoin.dart';
import 'package:todotest/Ui/clientProfile/bloc/client_profile_bloc.dart';
import 'package:todotest/Ui/clientProfile/bloc/client_profile_event.dart';
import 'package:todotest/Ui/clientProfile/bloc/client_profile_state.dart';
 import 'package:todotest/core/constent.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:todotest/core/style/base_color.dart';
import 'package:todotest/core/style/custom_loader.dart';

class ClientProfile extends StatefulWidget {
  @override
  _ClientProfileState createState() => _ClientProfileState();
}

class _ClientProfileState extends State<ClientProfile> {
  var scaffoldKey = GlobalKey<ScaffoldState>();

  
  
  
  final _bloc = sl<ClientProfileBloc>();

  
  @override
  void initState() {
    _bloc.add(GetUserProfile());
    _bloc.add(GetIsLogin());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
//      backgroundColor: AppColor.greenColor,
      body: BlocBuilder(
        cubit:_bloc,
        builder:(context,ClientProfileState state){
error(state.error);

return
    Stack(
      children: [
         SingleChildScrollView(
          child: state.user==null?Container():
          Column(
            children: [
              Column(
                children: <Widget>[
                  // background image and bottom contents
                  Column(
                    children: <Widget>[
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            height: 250,
                            color: AppColor.background2,
                          ),
                          Positioned(
                              top: 30.0,
                              // (background container size) - (circle height / 2)
                              child: Container(
                                height: 140,
                                child: Stack(
                                  children: [
                                    state.user.avatar.uri==null?Container(): CircleAvatar(
                                      radius: 60.0,
                                      backgroundColor: Colors.white,
                                      child: CircleAvatar(
                                        radius: 58.0,
                                        backgroundColor: Colors.white,
                                        backgroundImage: NetworkImage(
                                          state.user.avatar.uri
                                       ),
                                      ),
                                    ),

                                  ],
                                ),
                              )),
                          Positioned(
                              top: 190.0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Icon(
                                    Icons.location_on,
                                    color: Colors.white,
                                    size: 10,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(3.0),
                                    child: Text(
                                     state.user.address.name??"",
                                      style: TextStyle(
                                          fontSize: 10, color: Colors.white),
                                    ),
                                  ),
                                ],
                              )),
                          Positioned(
                              top: 170.0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(3.0),
                                    child: Text(
                                     state.user.name??"",
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    ),
                                  ),
                                ],
                              )),
                          Positioned(
                              top: 150.0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(3.0),
                                    child: Text(
                                      state.user.email??"",
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    ),
                                  ),
                                ],
                              ))
                        ],
                      ),

                    ],
                  ),
                  // Profile image
                ],
              ),
            ],
          ),
        ),
        state.isLoading?Center(child: loader(context: context),):Container()

      ],
    )
;
    
        }
                ),
    );
  }
  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          // timeInSecForIosWeb: 1,
          backgroundColor: containerColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
}
