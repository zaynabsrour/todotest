import 'package:bloc/bloc.dart';

import 'package:todotest/data/repository/irepository.dart';

import 'filter_page_event.dart';
import 'filter_page_state.dart';

class FilterPageBloc extends Bloc<FilterPageEvent, FilterPageState> {
  IRepository _repository;

  FilterPageBloc(this._repository) : super(FilterPageState.initail());
  @override
  FilterPageState get initialState => FilterPageState.initail();


  @override
  Stream<FilterPageState> mapEventToState(
      FilterPageEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }

    if (event is GetIsLogin) {
      try {
        final result = await _repository.getIsLogin();
        yield state.rebuild((b) => b..isLogin = result);

      } catch (e) {
        print(' Error $e');
        yield state.rebuild((b) => b
          ..error = ""
        )
        ;
      }
    }
/////// get todo status///////
    if (event is GetTodoInProgress) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..todoInProgress=null
          ..success=false
        );

        final data = await _repository.getTodoStatus(event.status);


        print('GetTodo Success data ${data}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..success=true
          ..todoInProgress.replace(data)
        );
      } catch (e) {
        print('GetTodo Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = e.error.toString()
          ..success=false

          ..todoInProgress=null
        );
      }
    }
    if (event is GetTodoPending) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..todoPending=null
          ..success=false
        );

        final data = await _repository.getTodoStatus(event.status);


        print('GetTodo Success data ${data}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..success=true
          ..todoPending.replace(data)
        );
      } catch (e) {
        print('GetTodo Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = e.error.toString()
          ..success=false

          ..todoPending=null
        );
      }
    }
    if (event is GetTodoDone) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..todoDone=null
          ..success=false
        );

        final data = await _repository.getTodoStatus(event.status);


        print('GetTodo Success data ${data}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..success=true
          ..todoDone.replace(data)
        );
      } catch (e) {
        print('GetTodo Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = e.error.toString()
          ..success=false

          ..todoDone=null
        );
      }
    }




  }
}
