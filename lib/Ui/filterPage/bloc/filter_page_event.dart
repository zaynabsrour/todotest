library filter_page_event;

import 'dart:convert';
import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:todotest/data/db_helper/entite/todo.dart';
part 'filter_page_event.g.dart';

abstract class FilterPageEvent {}

abstract class ClearError extends FilterPageEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}


abstract class GetTodoInProgress extends FilterPageEvent
    implements Built<GetTodoInProgress,GetTodoInProgressBuilder> {
  // fields go here

@nullable
String get status;

GetTodoInProgress._();

  factory GetTodoInProgress([updates(GetTodoInProgressBuilder b)]) = _$GetTodoInProgress;
}
abstract class GetTodoPending extends FilterPageEvent
    implements Built<GetTodoPending,GetTodoPendingBuilder> {
  // fields go here

  @nullable
  String get status;

  GetTodoPending._();

  factory GetTodoPending([updates(GetTodoPendingBuilder b)]) = _$GetTodoPending;
}
abstract class GetTodoDone extends FilterPageEvent
    implements Built<GetTodoDone,GetTodoDoneBuilder> {
  // fields go here

  @nullable
  String get status;

  GetTodoDone._();

  factory GetTodoDone([updates(GetTodoDoneBuilder b)]) = _$GetTodoDone;
}




abstract class GetIsLogin extends FilterPageEvent implements Built<GetIsLogin,GetIsLoginBuilder> {
  // fields go here



  GetIsLogin._();

  factory GetIsLogin([updates(GetIsLoginBuilder b)]) = _$GetIsLogin;
}










