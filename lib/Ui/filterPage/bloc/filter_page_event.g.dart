// GENERATED CODE - DO NOT MODIFY BY HAND

part of filter_page_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$GetTodoInProgress extends GetTodoInProgress {
  @override
  final String status;

  factory _$GetTodoInProgress(
          [void Function(GetTodoInProgressBuilder) updates]) =>
      (new GetTodoInProgressBuilder()..update(updates)).build();

  _$GetTodoInProgress._({this.status}) : super._();

  @override
  GetTodoInProgress rebuild(void Function(GetTodoInProgressBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetTodoInProgressBuilder toBuilder() =>
      new GetTodoInProgressBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetTodoInProgress && status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(0, status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetTodoInProgress')
          ..add('status', status))
        .toString();
  }
}

class GetTodoInProgressBuilder
    implements Builder<GetTodoInProgress, GetTodoInProgressBuilder> {
  _$GetTodoInProgress _$v;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  GetTodoInProgressBuilder();

  GetTodoInProgressBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetTodoInProgress other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetTodoInProgress;
  }

  @override
  void update(void Function(GetTodoInProgressBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetTodoInProgress build() {
    final _$result = _$v ?? new _$GetTodoInProgress._(status: status);
    replace(_$result);
    return _$result;
  }
}

class _$GetTodoPending extends GetTodoPending {
  @override
  final String status;

  factory _$GetTodoPending([void Function(GetTodoPendingBuilder) updates]) =>
      (new GetTodoPendingBuilder()..update(updates)).build();

  _$GetTodoPending._({this.status}) : super._();

  @override
  GetTodoPending rebuild(void Function(GetTodoPendingBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetTodoPendingBuilder toBuilder() =>
      new GetTodoPendingBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetTodoPending && status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(0, status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetTodoPending')
          ..add('status', status))
        .toString();
  }
}

class GetTodoPendingBuilder
    implements Builder<GetTodoPending, GetTodoPendingBuilder> {
  _$GetTodoPending _$v;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  GetTodoPendingBuilder();

  GetTodoPendingBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetTodoPending other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetTodoPending;
  }

  @override
  void update(void Function(GetTodoPendingBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetTodoPending build() {
    final _$result = _$v ?? new _$GetTodoPending._(status: status);
    replace(_$result);
    return _$result;
  }
}

class _$GetTodoDone extends GetTodoDone {
  @override
  final String status;

  factory _$GetTodoDone([void Function(GetTodoDoneBuilder) updates]) =>
      (new GetTodoDoneBuilder()..update(updates)).build();

  _$GetTodoDone._({this.status}) : super._();

  @override
  GetTodoDone rebuild(void Function(GetTodoDoneBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetTodoDoneBuilder toBuilder() => new GetTodoDoneBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetTodoDone && status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(0, status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetTodoDone')..add('status', status))
        .toString();
  }
}

class GetTodoDoneBuilder implements Builder<GetTodoDone, GetTodoDoneBuilder> {
  _$GetTodoDone _$v;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  GetTodoDoneBuilder();

  GetTodoDoneBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetTodoDone other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetTodoDone;
  }

  @override
  void update(void Function(GetTodoDoneBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetTodoDone build() {
    final _$result = _$v ?? new _$GetTodoDone._(status: status);
    replace(_$result);
    return _$result;
  }
}

class _$GetIsLogin extends GetIsLogin {
  factory _$GetIsLogin([void Function(GetIsLoginBuilder) updates]) =>
      (new GetIsLoginBuilder()..update(updates)).build();

  _$GetIsLogin._() : super._();

  @override
  GetIsLogin rebuild(void Function(GetIsLoginBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetIsLoginBuilder toBuilder() => new GetIsLoginBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetIsLogin;
  }

  @override
  int get hashCode {
    return 118379423;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetIsLogin').toString();
  }
}

class GetIsLoginBuilder implements Builder<GetIsLogin, GetIsLoginBuilder> {
  _$GetIsLogin _$v;

  GetIsLoginBuilder();

  @override
  void replace(GetIsLogin other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetIsLogin;
  }

  @override
  void update(void Function(GetIsLoginBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetIsLogin build() {
    final _$result = _$v ?? new _$GetIsLogin._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
