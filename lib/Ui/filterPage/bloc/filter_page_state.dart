library filter_page_state;

import 'dart:convert';

import 'package:built_value/built_value.dart';

import 'package:todotest/model/user/user.dart';
import 'package:todotest/model/user_response/user_response.dart';

import 'package:built_collection/built_collection.dart';
 import 'package:todotest/data/db_helper/entite/todo.dart';
part 'filter_page_state.g.dart';

abstract class FilterPageState
    implements Built<FilterPageState, FilterPageStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;
  bool get isLoadingSub;

  @nullable
  // BaseCart get cart;
  BuiltList<Todo> get todoDone;
  @nullable
  // BaseCart get cart;
  BuiltList<Todo> get todoInProgress;
  @nullable
  // BaseCart get cart;
  BuiltList<Todo> get todoPending;
  bool get isLogin;




  bool get success;
  //bool get successfb;


  FilterPageState._();

  factory FilterPageState([updates(FilterPageStateBuilder b)]) =
  _$FilterPageState;

  factory FilterPageState.initail() {
    return FilterPageState((b) => b
      ..error = ""
      ..isLoading = false
      ..isLoadingSub = false
      ..success = false

        ..todoDone.replace([])
        ..todoInProgress.replace([])
        ..todoPending.replace([])

        ..isLogin=false
    );
  }
}
