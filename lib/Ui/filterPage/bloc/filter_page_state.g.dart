// GENERATED CODE - DO NOT MODIFY BY HAND

part of filter_page_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FilterPageState extends FilterPageState {
  @override
  final String error;
  @override
  final bool isLoading;
  @override
  final bool isLoadingSub;
  @override
  final BuiltList<Todo> todoDone;
  @override
  final BuiltList<Todo> todoInProgress;
  @override
  final BuiltList<Todo> todoPending;
  @override
  final bool isLogin;
  @override
  final bool success;

  factory _$FilterPageState([void Function(FilterPageStateBuilder) updates]) =>
      (new FilterPageStateBuilder()..update(updates)).build();

  _$FilterPageState._(
      {this.error,
      this.isLoading,
      this.isLoadingSub,
      this.todoDone,
      this.todoInProgress,
      this.todoPending,
      this.isLogin,
      this.success})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('FilterPageState', 'error');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('FilterPageState', 'isLoading');
    }
    if (isLoadingSub == null) {
      throw new BuiltValueNullFieldError('FilterPageState', 'isLoadingSub');
    }
    if (isLogin == null) {
      throw new BuiltValueNullFieldError('FilterPageState', 'isLogin');
    }
    if (success == null) {
      throw new BuiltValueNullFieldError('FilterPageState', 'success');
    }
  }

  @override
  FilterPageState rebuild(void Function(FilterPageStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FilterPageStateBuilder toBuilder() =>
      new FilterPageStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FilterPageState &&
        error == other.error &&
        isLoading == other.isLoading &&
        isLoadingSub == other.isLoadingSub &&
        todoDone == other.todoDone &&
        todoInProgress == other.todoInProgress &&
        todoPending == other.todoPending &&
        isLogin == other.isLogin &&
        success == other.success;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, error.hashCode), isLoading.hashCode),
                            isLoadingSub.hashCode),
                        todoDone.hashCode),
                    todoInProgress.hashCode),
                todoPending.hashCode),
            isLogin.hashCode),
        success.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('FilterPageState')
          ..add('error', error)
          ..add('isLoading', isLoading)
          ..add('isLoadingSub', isLoadingSub)
          ..add('todoDone', todoDone)
          ..add('todoInProgress', todoInProgress)
          ..add('todoPending', todoPending)
          ..add('isLogin', isLogin)
          ..add('success', success))
        .toString();
  }
}

class FilterPageStateBuilder
    implements Builder<FilterPageState, FilterPageStateBuilder> {
  _$FilterPageState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _isLoadingSub;
  bool get isLoadingSub => _$this._isLoadingSub;
  set isLoadingSub(bool isLoadingSub) => _$this._isLoadingSub = isLoadingSub;

  ListBuilder<Todo> _todoDone;
  ListBuilder<Todo> get todoDone =>
      _$this._todoDone ??= new ListBuilder<Todo>();
  set todoDone(ListBuilder<Todo> todoDone) => _$this._todoDone = todoDone;

  ListBuilder<Todo> _todoInProgress;
  ListBuilder<Todo> get todoInProgress =>
      _$this._todoInProgress ??= new ListBuilder<Todo>();
  set todoInProgress(ListBuilder<Todo> todoInProgress) =>
      _$this._todoInProgress = todoInProgress;

  ListBuilder<Todo> _todoPending;
  ListBuilder<Todo> get todoPending =>
      _$this._todoPending ??= new ListBuilder<Todo>();
  set todoPending(ListBuilder<Todo> todoPending) =>
      _$this._todoPending = todoPending;

  bool _isLogin;
  bool get isLogin => _$this._isLogin;
  set isLogin(bool isLogin) => _$this._isLogin = isLogin;

  bool _success;
  bool get success => _$this._success;
  set success(bool success) => _$this._success = success;

  FilterPageStateBuilder();

  FilterPageStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _isLoading = _$v.isLoading;
      _isLoadingSub = _$v.isLoadingSub;
      _todoDone = _$v.todoDone?.toBuilder();
      _todoInProgress = _$v.todoInProgress?.toBuilder();
      _todoPending = _$v.todoPending?.toBuilder();
      _isLogin = _$v.isLogin;
      _success = _$v.success;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FilterPageState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$FilterPageState;
  }

  @override
  void update(void Function(FilterPageStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$FilterPageState build() {
    _$FilterPageState _$result;
    try {
      _$result = _$v ??
          new _$FilterPageState._(
              error: error,
              isLoading: isLoading,
              isLoadingSub: isLoadingSub,
              todoDone: _todoDone?.build(),
              todoInProgress: _todoInProgress?.build(),
              todoPending: _todoPending?.build(),
              isLogin: isLogin,
              success: success);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'todoDone';
        _todoDone?.build();
        _$failedField = 'todoInProgress';
        _todoInProgress?.build();
        _$failedField = 'todoPending';
        _todoPending?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'FilterPageState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
