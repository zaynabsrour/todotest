import 'package:flutter/material.dart';
 import 'package:todotest/core/constent.dart';
import 'package:todotest/Ui/filterPage/bloc/filter_page_bloc.dart';
import 'package:todotest/Ui/filterPage/bloc/filter_page_state.dart';
import 'package:todotest/Ui/filterPage/bloc/filter_page_event.dart';

import 'package:todotest/injectoin.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
 import 'package:fluttertoast/fluttertoast.dart';
 import 'package:todotest/core/style/base_color.dart';
import 'package:todotest/core/style/custom_loader.dart';
  import 'package:todotest/Ui/filterPage/items/items.dart';
import 'package:todotest/core/style/dialogcustom.dart';
 class FilterPage extends StatefulWidget {

  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  String dropDownValue;
  var _addressDetailsController=new TextEditingController();
  var _postCodeController=new TextEditingController();
  List<String> list = [
    'german',
    'france',
    'Dubai',
  ];
  List<Items> listItems=[];
  Items item;
  String location="";
  double longitude=0.0;
  double latitude=0.0;
  final _bloc = sl<FilterPageBloc>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc.add(GetTodoPending((b)=>b
    ..status="pending"
    ));
    _bloc.add(GetTodoInProgress((b)=>b
      ..status="inProgress"
    ));
    _bloc.add(GetTodoDone((b)=>b
      ..status="done"
    ));
    _bloc.add(GetIsLogin());
  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return BlocBuilder(
        cubit: _bloc,
        builder: (BuildContext context, FilterPageState state)
    {


      error(state.error);
      return Scaffold(
        body:
            Stack(
              children: [
                SingleChildScrollView(
                  child:Column(
                    children: [
                      Text("Todo Pending"),
                      state.todoPending==null ?Container(child: Text("Empty"),):

                      ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),

                        itemCount:state.todoPending.length,
                        itemBuilder: (context, index) {
                          return

                            ListTile(

                                title:Text(state.todoPending[index].title) ,
                                subtitle: Text(state.todoPending[index].description),



                            )

                          ;
                          //TodoCard(state.todo[index],_bloc,state.isLogin);
                        },
                      ),
                      Divider(),
                      Text("Todo Done"),
                      state.todoDone==null ?Container(child: Text("Empty"),):

                      ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),

                        itemCount:state.todoDone.length,
                        itemBuilder: (context, index) {
                          return

                            ListTile(

                              title:Text(state.todoDone[index].title) ,
                              subtitle: Text(state.todoDone[index].description),



                            )

                          ;
                          //TodoCard(state.todo[index],_bloc,state.isLogin);
                        },
                      ),
                      Divider(),
                      Text("Todo In Progress"),
                      state.todoInProgress==null ?Container(child: Text("Empty"),):

                      ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),

                        itemCount:state.todoInProgress.length,
                        itemBuilder: (context, index) {
                          return

                            ListTile(

                              title:Text(state.todoInProgress[index].title) ,
                              subtitle: Text(state.todoInProgress[index].description),




                              )
                          ;
                          //TodoCard(state.todo[index],_bloc,state.isLogin);
                        },
                      ),


                    ],
                  )


                ),
                state.isLoading?Center(child: loader(context: context),):Container()
              ],
            )

      );
    });
  }
  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode ,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          // timeInSecForIosWeb: 1,
          backgroundColor: containerColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
  // showSheet() {
  //   showModalBottomSheet(
  //       isScrollControlled: true,
  //       shape: RoundedRectangleBorder(
  //         borderRadius: BorderRadius.circular(25.0),
  //       ),
  //       context: context,
  //       builder: (BuildContext context) {
  //         return SelectPayment();
  //       });
  // }
}
