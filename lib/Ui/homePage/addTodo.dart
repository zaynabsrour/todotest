import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:todotest/Ui/homePage/bloc/home_page_bloc.dart';
import 'package:todotest/Ui/homePage/bloc/home_page_event.dart';
import 'package:todotest/Ui/homePage/bloc/home_page_state.dart';
 import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todotest/Ui/homePage/home_page.dart';
import 'package:todotest/core/style/custom_loader.dart';
import 'package:todotest/data/db_helper/entite/todo.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:todotest/core/style/base_color.dart';
class AddTodo extends StatefulWidget {
  final HomePageBloc bloc;


  AddTodo(this.bloc);
  @override
  _AddTodoState createState() => _AddTodoState();
}

class _AddTodoState extends State<AddTodo> {

  String status = "";
  var _titleController=new TextEditingController();
  var _descriptionController=new TextEditingController();
 String date="";
   @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        cubit: widget.bloc,
        builder: (BuildContext context, HomePageState state) {

          return
              SingleChildScrollView(
                  child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(16),
                    topLeft: Radius.circular(16)),
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: MediaQuery.of(context).size.height*0.85,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(16),
                            topLeft: Radius.circular(16))),
                    child: Stack(
                      children: [
                        Column(
                          children: [
                            Padding(
                                padding: EdgeInsets.all(16),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        Icons.arrow_back,
                                        color: Colors.orange,
                                        size: 30,
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                    InkWell(
                                        onTap: () {
                                          print("status: "+status);
                                           if(_titleController.text.isEmpty){error("title required");}
                                           else
                                             if(_descriptionController.text.isEmpty){error("description required");}
                                             else
                                               if(status==""){error("stataus required");}
                                               else
                                          widget.bloc.add(Add((b) => b
                                                ..todo=Todo(
                                                  title: _titleController.text,
                                                  description: _descriptionController.text,
                                                  status: status,
                                                  date: DateTime.now().toString()

                                                )

                                              //..search = _searchController.text

                                              //..SortByReview = SortResult
                                              ));
                                          Navigator.pop(context);
                                        },
                                        child: Text(("Add")))
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.all(16),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Title"),
                                    Container(width: 6,),
                                    Container(
                                      height: 50,
                                      width: 150,
                                      child:   TextFormField(

                                        controller: _titleController,
                                        textAlign: TextAlign.start,

                                        keyboardType: TextInputType.text,
                                      ),
                                    )


                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.all(16),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Description"),
                                Container(width: 6,),
                                Container(
                                  height: 50,
                                  width: 150,
                                  child:
                                    TextFormField(
                                      controller: _descriptionController,
                                      textAlign: TextAlign.start,

                                      keyboardType: TextInputType.text,
                                    )),

                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.all(16),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Status"),
                                    DropdownButton<String>(
                                      underline: Container(),
                                      value: null,
                                      //isExpanded: true,
                                      hint: Container(
                                        child: Text(
                                          //  AppLocalizations.of(context)
                                          // .translate(
                                          status == ""
                                              ? "Status"
                                              : status,
                                          //),
                                          // style: TextStyle(
                                          //     fontSize: 12,
                                          //     color: Colors.grey),
                                        ),
                                      ),
                                      isDense: true,

                                      onChanged: (String newValue) {
                                        setState(() {
                                          status = newValue;
                                        });
                                      },
                                      items: ["pending", "inProgress", "done"]
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                            value: value,
                                            child: Container(
                                              margin: EdgeInsets.only(
                                                  right: 2, left: 12),
                                              child: Text(
                                                value,
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                            ));
                                      }).toList(),
                                    ),
                                  ],
                                )),





                          ],
                        ),
                        state.isLoadingSub
                            ? Center(
                          child: loader(context: context),
                        )
                            : Container()
                      ],
                    )),
              ))

           ;
        });
  }
  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg:errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          // timeInSecForIosWeb: 1,
          backgroundColor: containerColor,
          textColor: Colors.white,
          fontSize: 16.0);
      widget.bloc.add(ClearError());
    }
  }
}
