import 'package:bloc/bloc.dart';

import 'package:todotest/data/repository/irepository.dart';

import 'home_page_event.dart';
import 'home_page_state.dart';

class HomePageBloc extends Bloc<HomePageEvent, HomePageState> {
  IRepository _repository;

  HomePageBloc(this._repository) : super(HomePageState.initail());
  @override
  HomePageState get initialState => HomePageState.initail();


  @override
  Stream<HomePageState> mapEventToState(
      HomePageEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }

    if (event is GetIsLogin) {
      try {
        final result = await _repository.getIsLogin();
        yield state.rebuild((b) => b..isLogin = result);

      } catch (e) {
        print(' Error $e');
        yield state.rebuild((b) => b
          ..error = ""
        )
        ;
      }
    }
/////// get todo ///////
    if (event is GetTodo) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..todo=null
          ..success=false
        );

        final data = await _repository.getTodo();


        print('GetTodo Success data ${data}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..success=true
          ..todo.replace(data)
        );
      } catch (e) {
        print('GetTodo Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = e.error.toString()
          ..success=false

          ..todo=null
        );
      }
    }
    /////***** delete todo ****////
    if(event is Delete){
      yield state.rebuild((b) => b..isLoading = true);

      try {

        final result = await _repository.deleteTodo(event.id);
        yield state.rebuild((b) => b
          ..isLoading = false
        );




        try {

          final result = await _repository.getTodo();
          yield state.rebuild((b) => b
            ..isLoading = false
            ..todo.replace(result)
          );


        } catch (e) {
          print('$e');
          yield state.rebuild((b) => b
            ..isLoading = false
            ..error = "Something went wrong");
        }

      } catch (e) {
        print('$e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    /////***** update todo ****////
    if(event is Update){
      yield state.rebuild((b) => b..isLoading = true);

      try {

        final result = await _repository.updateTodo(event.todo);
        yield state.rebuild((b) => b
          ..isLoading = false
        );

        try {

          final result = await _repository.getTodo();
           yield state.rebuild((b) => b
            ..isLoading = false
            ..todo.replace(result)
          );


        } catch (e) {
          print('$e');
          yield state.rebuild((b) => b
            ..isLoading = false
            ..error = "Something went wrong");
        }

      } catch (e) {
        print('$e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    ///// add todo ////
    if (event is Add) {
      try {
        await _repository.insertTodo(event.todo);
        final result = await _repository.getTodo();
        yield state.rebuild((b) => b
          ..isLoading = false
          ..todo.replace(result)
          ..error="Added Successfully"
        );
      } catch (e) {
        print('$e');
        yield state.rebuild((b) =>
        b
          ..isLoading = false
          ..error = "");
      }
    }




  }
}
