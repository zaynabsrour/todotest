library home_page_event;

import 'dart:convert';
import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:todotest/data/db_helper/entite/todo.dart';
part 'home_page_event.g.dart';

abstract class HomePageEvent {}

abstract class ClearError extends HomePageEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}


abstract class GetTodo extends HomePageEvent
    implements Built<GetTodo, GetTodoBuilder> {
  // fields go here



  GetTodo._();

  factory GetTodo([updates(GetTodoBuilder b)]) = _$GetTodo;
}




abstract class GetIsLogin extends HomePageEvent implements Built<GetIsLogin,GetIsLoginBuilder> {
  // fields go here



  GetIsLogin._();

  factory GetIsLogin([updates(GetIsLoginBuilder b)]) = _$GetIsLogin;
}
abstract class Delete extends HomePageEvent
    implements Built<Delete, DeleteBuilder> {
  // fields go here
  @nullable
  int get id;


  Delete._();

  factory Delete([updates(DeleteBuilder b)]) = _$Delete;
}

abstract class Add extends HomePageEvent implements Built<Add, AddBuilder> {
  // fields go here
  @nullable
  Todo get todo;

  Add._();

  factory Add([updates(AddBuilder b)]) = _$Add;
}
abstract class Update extends HomePageEvent
    implements Built<Update, UpdateBuilder> {
  // fields go here
  @nullable
  Todo get todo;



  Update._();

  factory Update([updates(UpdateBuilder b)]) = _$Update;
}











