// GENERATED CODE - DO NOT MODIFY BY HAND

part of home_page_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$GetTodo extends GetTodo {
  factory _$GetTodo([void Function(GetTodoBuilder) updates]) =>
      (new GetTodoBuilder()..update(updates)).build();

  _$GetTodo._() : super._();

  @override
  GetTodo rebuild(void Function(GetTodoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetTodoBuilder toBuilder() => new GetTodoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetTodo;
  }

  @override
  int get hashCode {
    return 1037994528;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetTodo').toString();
  }
}

class GetTodoBuilder implements Builder<GetTodo, GetTodoBuilder> {
  _$GetTodo _$v;

  GetTodoBuilder();

  @override
  void replace(GetTodo other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetTodo;
  }

  @override
  void update(void Function(GetTodoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetTodo build() {
    final _$result = _$v ?? new _$GetTodo._();
    replace(_$result);
    return _$result;
  }
}

class _$GetIsLogin extends GetIsLogin {
  factory _$GetIsLogin([void Function(GetIsLoginBuilder) updates]) =>
      (new GetIsLoginBuilder()..update(updates)).build();

  _$GetIsLogin._() : super._();

  @override
  GetIsLogin rebuild(void Function(GetIsLoginBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetIsLoginBuilder toBuilder() => new GetIsLoginBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetIsLogin;
  }

  @override
  int get hashCode {
    return 118379423;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetIsLogin').toString();
  }
}

class GetIsLoginBuilder implements Builder<GetIsLogin, GetIsLoginBuilder> {
  _$GetIsLogin _$v;

  GetIsLoginBuilder();

  @override
  void replace(GetIsLogin other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetIsLogin;
  }

  @override
  void update(void Function(GetIsLoginBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetIsLogin build() {
    final _$result = _$v ?? new _$GetIsLogin._();
    replace(_$result);
    return _$result;
  }
}

class _$Delete extends Delete {
  @override
  final int id;

  factory _$Delete([void Function(DeleteBuilder) updates]) =>
      (new DeleteBuilder()..update(updates)).build();

  _$Delete._({this.id}) : super._();

  @override
  Delete rebuild(void Function(DeleteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DeleteBuilder toBuilder() => new DeleteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Delete && id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(0, id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Delete')..add('id', id)).toString();
  }
}

class DeleteBuilder implements Builder<Delete, DeleteBuilder> {
  _$Delete _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  DeleteBuilder();

  DeleteBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Delete other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Delete;
  }

  @override
  void update(void Function(DeleteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Delete build() {
    final _$result = _$v ?? new _$Delete._(id: id);
    replace(_$result);
    return _$result;
  }
}

class _$Add extends Add {
  @override
  final Todo todo;

  factory _$Add([void Function(AddBuilder) updates]) =>
      (new AddBuilder()..update(updates)).build();

  _$Add._({this.todo}) : super._();

  @override
  Add rebuild(void Function(AddBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddBuilder toBuilder() => new AddBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Add && todo == other.todo;
  }

  @override
  int get hashCode {
    return $jf($jc(0, todo.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Add')..add('todo', todo)).toString();
  }
}

class AddBuilder implements Builder<Add, AddBuilder> {
  _$Add _$v;

  Todo _todo;
  Todo get todo => _$this._todo;
  set todo(Todo todo) => _$this._todo = todo;

  AddBuilder();

  AddBuilder get _$this {
    if (_$v != null) {
      _todo = _$v.todo;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Add other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Add;
  }

  @override
  void update(void Function(AddBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Add build() {
    final _$result = _$v ?? new _$Add._(todo: todo);
    replace(_$result);
    return _$result;
  }
}

class _$Update extends Update {
  @override
  final Todo todo;

  factory _$Update([void Function(UpdateBuilder) updates]) =>
      (new UpdateBuilder()..update(updates)).build();

  _$Update._({this.todo}) : super._();

  @override
  Update rebuild(void Function(UpdateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateBuilder toBuilder() => new UpdateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Update && todo == other.todo;
  }

  @override
  int get hashCode {
    return $jf($jc(0, todo.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Update')..add('todo', todo))
        .toString();
  }
}

class UpdateBuilder implements Builder<Update, UpdateBuilder> {
  _$Update _$v;

  Todo _todo;
  Todo get todo => _$this._todo;
  set todo(Todo todo) => _$this._todo = todo;

  UpdateBuilder();

  UpdateBuilder get _$this {
    if (_$v != null) {
      _todo = _$v.todo;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Update other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Update;
  }

  @override
  void update(void Function(UpdateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Update build() {
    final _$result = _$v ?? new _$Update._(todo: todo);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
