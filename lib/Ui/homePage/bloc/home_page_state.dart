library home_page_state;

import 'dart:convert';

import 'package:built_value/built_value.dart';

import 'package:todotest/model/user/user.dart';
import 'package:todotest/model/user_response/user_response.dart';

 import 'package:built_collection/built_collection.dart';

import 'package:todotest/data/db_helper/entite/todo.dart';
part 'home_page_state.g.dart';

abstract class HomePageState
    implements Built<HomePageState, HomePageStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;
  bool get isLoadingSub;

  @nullable
  // BaseCart get cart;
  BuiltList<Todo> get todo;
  bool get isLogin;




  bool get success;
  //bool get successfb;


  HomePageState._();

  factory HomePageState([updates(HomePageStateBuilder b)]) =
  _$HomePageState;

  factory HomePageState.initail() {
    return HomePageState((b) => b
      ..error = ""
      ..isLoading = false
      ..isLoadingSub = false
      ..success = false

        ..todo.replace([])

        ..isLogin=false
    );
  }
}
