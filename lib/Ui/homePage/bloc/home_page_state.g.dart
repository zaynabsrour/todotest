// GENERATED CODE - DO NOT MODIFY BY HAND

part of home_page_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HomePageState extends HomePageState {
  @override
  final String error;
  @override
  final bool isLoading;
  @override
  final bool isLoadingSub;
  @override
  final BuiltList<Todo> todo;
  @override
  final bool isLogin;
  @override
  final bool success;

  factory _$HomePageState([void Function(HomePageStateBuilder) updates]) =>
      (new HomePageStateBuilder()..update(updates)).build();

  _$HomePageState._(
      {this.error,
      this.isLoading,
      this.isLoadingSub,
      this.todo,
      this.isLogin,
      this.success})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('HomePageState', 'error');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('HomePageState', 'isLoading');
    }
    if (isLoadingSub == null) {
      throw new BuiltValueNullFieldError('HomePageState', 'isLoadingSub');
    }
    if (isLogin == null) {
      throw new BuiltValueNullFieldError('HomePageState', 'isLogin');
    }
    if (success == null) {
      throw new BuiltValueNullFieldError('HomePageState', 'success');
    }
  }

  @override
  HomePageState rebuild(void Function(HomePageStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HomePageStateBuilder toBuilder() => new HomePageStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HomePageState &&
        error == other.error &&
        isLoading == other.isLoading &&
        isLoadingSub == other.isLoadingSub &&
        todo == other.todo &&
        isLogin == other.isLogin &&
        success == other.success;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, error.hashCode), isLoading.hashCode),
                    isLoadingSub.hashCode),
                todo.hashCode),
            isLogin.hashCode),
        success.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('HomePageState')
          ..add('error', error)
          ..add('isLoading', isLoading)
          ..add('isLoadingSub', isLoadingSub)
          ..add('todo', todo)
          ..add('isLogin', isLogin)
          ..add('success', success))
        .toString();
  }
}

class HomePageStateBuilder
    implements Builder<HomePageState, HomePageStateBuilder> {
  _$HomePageState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _isLoadingSub;
  bool get isLoadingSub => _$this._isLoadingSub;
  set isLoadingSub(bool isLoadingSub) => _$this._isLoadingSub = isLoadingSub;

  ListBuilder<Todo> _todo;
  ListBuilder<Todo> get todo => _$this._todo ??= new ListBuilder<Todo>();
  set todo(ListBuilder<Todo> todo) => _$this._todo = todo;

  bool _isLogin;
  bool get isLogin => _$this._isLogin;
  set isLogin(bool isLogin) => _$this._isLogin = isLogin;

  bool _success;
  bool get success => _$this._success;
  set success(bool success) => _$this._success = success;

  HomePageStateBuilder();

  HomePageStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _isLoading = _$v.isLoading;
      _isLoadingSub = _$v.isLoadingSub;
      _todo = _$v.todo?.toBuilder();
      _isLogin = _$v.isLogin;
      _success = _$v.success;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HomePageState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$HomePageState;
  }

  @override
  void update(void Function(HomePageStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$HomePageState build() {
    _$HomePageState _$result;
    try {
      _$result = _$v ??
          new _$HomePageState._(
              error: error,
              isLoading: isLoading,
              isLoadingSub: isLoadingSub,
              todo: _todo?.build(),
              isLogin: isLogin,
              success: success);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'todo';
        _todo?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'HomePageState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
