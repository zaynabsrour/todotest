import 'package:flutter/material.dart';
 import 'package:todotest/Ui/filterPage/page/filter_page.dart';

import 'package:todotest/Ui/clientProfile/clientProfile.dart';
 import 'package:todotest/Ui/homePage/todoCard.dart';
import 'package:todotest/Ui/homePage/widgets.dart';
import 'package:todotest/Ui/homePage/addTodo.dart';
import 'package:built_collection/built_collection.dart';
import 'package:todotest/data/db_helper/entite/todo.dart';
  import 'package:todotest/core/constent.dart';
import 'package:todotest/injectoin.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todotest/Ui/homePage/bloc/home_page_bloc.dart';
import 'package:todotest/Ui/homePage/bloc/home_page_event.dart';
import 'package:todotest/Ui/homePage/bloc/home_page_state.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:todotest/core/style/base_color.dart';
import 'package:todotest/core/style/custom_loader.dart';
 import 'package:todotest/Ui/homePage/editTodo.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  final _bloc = sl<HomePageBloc>();
  int index = 0;

  var _selectedPage = [];
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc.add(GetIsLogin());
    _bloc.add(GetTodo());

  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;



    return  BlocBuilder(
        cubit: _bloc,
        builder: (context,HomePageState state)
    {
      error(state.error);
      return Scaffold(
        key: scaffoldKey,


        body:
        Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                      padding: const EdgeInsets.only(
                          right: 18.0, left: 18, top: 10),
                      child:
                          GestureDetector(
                            onTap: (){
                              setState(() {

                                showSheet(_bloc);
                              });

                              },
                            child:

                      Container(
                        height: 50,
                        width: 80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: Colors.grey
                        ),
                        child:Center(
                         child:   Text(

                           'Create New Todo',

                           style: TextStyle(
                               color: Colors.black,
                               fontSize: 14,
                               fontWeight: FontWeight.bold),
                         )
                        )
                       )),

                        ),
                  SizedBox(
                    height: 10,
                  ),
                  state.todo==null ?Container(): Padding(
                      padding: const EdgeInsets.only(
                          right: 18.0, left: 18, top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(

                              'Todo',

                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),

                        ],
                      )),
                  state.todo==null ?Container():   Padding(
                      padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                      child: CustomDivider(size)),
                  state.todo==null ?Container():

                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),

                      itemCount:state.todo.length,
                      itemBuilder: (context, index) {
                        return

                        ListTile(

                          title:Text(state.todo[index].title) ,
                           subtitle: Text(state.todo[index].description),
                          trailing:
                          GestureDetector(
                            onTap: (){
                              _bloc.add(Delete((b)=>b
                                ..id= state.todo[index].id));

                            },
                            child:   Icon(Icons.close,color:Colors.red),
                          ),

                          leading:   GestureDetector(
                            onTap: (){
                             editshowSheet(_bloc, state.todo[index]);
                            },
                            child:   Icon(Icons.edit),
                          )
                        )

                          ;
                          //TodoCard(state.todo[index],_bloc,state.isLogin);
                      },
                    ),










                ],
              ),
            ),
            state.isLoading?Center(child: loader(context:context),):Container()
          ],
        )

      );
    });
  }

  InputDecoration fieldDecoration(String hint) {
    return InputDecoration(
      hintText: hint,
      hintStyle: TextStyle(fontSize: 16, color: AppColor.textGreyColor),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(
          width: 0,
          style: BorderStyle.none,
        ),
      ),
      filled: true,
      contentPadding: EdgeInsets.all(16),
      fillColor: AppColor.fillGreyColor,
    );
  }
  showSheet(HomePageBloc bloc) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        context: context,
        builder: (BuildContext context) {
          return AddTodo(bloc);
        });
  }
  editshowSheet(HomePageBloc bloc,Todo todo) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        context: context,
        builder: (BuildContext context) {
          return EditTodo(bloc,todo);
        });
  }
  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg:errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          // timeInSecForIosWeb: 1,
          backgroundColor: containerColor,
          textColor: Colors.white,
          fontSize: 16.0);
     _bloc.add(ClearError());
    }
  }

}
