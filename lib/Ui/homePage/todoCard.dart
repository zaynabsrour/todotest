import 'package:flutter/material.dart';

import 'package:todotest/core/constent.dart';
import 'package:todotest/core/style/dialogcustom.dart';
 import 'package:built_collection/built_collection.dart';
 import 'package:todotest/Ui/homePage/bloc/home_page_bloc.dart';
import 'package:todotest/Ui/homePage/bloc/home_page_event.dart';
import 'package:todotest/data/db_helper/entite/todo.dart';
class TodoCard extends StatelessWidget {
  final Todo todo;


  final HomePageBloc bloc;
  final bool isLogin;
  TodoCard(this.todo,this.bloc,this.isLogin);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {

        },
        child: Container(
          height: 220,
          width: 150,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: ListTile(
            title:Text(todo.title??"") ,
            subtitle: Text(todo.description??""),
            leading: Row(
              children: [
                GestureDetector(
                  onTap: (){
                    bloc.add(Delete((b)=>b
                      ..id= todo.id));

                  },
                  child:   Icon(Icons.remove),
                ),
                GestureDetector(
                  onTap: (){
                    bloc.add(
                        Update((b) =>
                        b
                          ..todo = Todo(
                              id: todo
                                  .id,

                              title: todo.title,
                              description:todo.description,
                              date: DateTime.now().toString()

                          )
                        ));
                  },
                  child:   Icon(Icons.edit),
                )

              ],
            ),
          )
        ),
      ),
    );
  }

}
