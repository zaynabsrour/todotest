import 'package:flutter/material.dart';
 import 'package:todotest/core/constent.dart';

Widget CustomDivider(Size size) {
  return Row(
    children: [
      Container(
        height: 3,
        color: AppColor.yallowDark,
        width: 80,
      ),
      Container(
        height: 1,
        color: Colors.grey,
        width: size.width - 120,
      ),
    ],
  );
}

Widget SectionHead(String title, context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        title,
        style: TextStyle(
            color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold),
      ),
      Text(

          'seeAll',

        style: TextStyle(color: AppColor.greenColor, fontSize: 10),
      ),
    ],
  );
}
