import 'package:bloc/bloc.dart';

import 'package:todotest/data/repository/irepository.dart';

import 'main_page_event.dart';
import 'main_page_state.dart';

class MainPageBloc extends Bloc<MainPageEvent, MainPageState> {
  IRepository _repository;

  MainPageBloc(this._repository) : super(MainPageState.initail());
  @override
  MainPageState get initialState => MainPageState.initail();


  @override
  Stream<MainPageState> mapEventToState(
      MainPageEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }

    if (event is GetIsLogin) {
      try {
        final result = await _repository.getIsLogin();
        yield state.rebuild((b) => b..isLogin = result);

      } catch (e) {
        print(' Error $e');
        yield state.rebuild((b) => b
          ..error = ""
        )
        ;
      }
    }





  }
}
