library main_page_state;

import 'dart:convert';

import 'package:built_value/built_value.dart';
  import 'package:todotest/model/user/user.dart';
import 'package:todotest/model/user_response/user_response.dart';
 import 'package:built_collection/built_collection.dart';
 part 'main_page_state.g.dart';

abstract class MainPageState
    implements Built<MainPageState, MainPageStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;


  bool get isLogin;


  bool get success;
  //bool get successfb;


  MainPageState._();

  factory MainPageState([updates(MainPageStateBuilder b)]) =
  _$MainPageState;

  factory MainPageState.initail() {
    return MainPageState((b) => b
      ..error = ""
      ..isLoading = false
      ..success = false

        ..isLogin=false
    );
  }
}
