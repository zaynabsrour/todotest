// GENERATED CODE - DO NOT MODIFY BY HAND

part of main_page_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MainPageState extends MainPageState {
  @override
  final String error;
  @override
  final bool isLoading;
  @override
  final bool isLogin;
  @override
  final bool success;

  factory _$MainPageState([void Function(MainPageStateBuilder) updates]) =>
      (new MainPageStateBuilder()..update(updates)).build();

  _$MainPageState._({this.error, this.isLoading, this.isLogin, this.success})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('MainPageState', 'error');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('MainPageState', 'isLoading');
    }
    if (isLogin == null) {
      throw new BuiltValueNullFieldError('MainPageState', 'isLogin');
    }
    if (success == null) {
      throw new BuiltValueNullFieldError('MainPageState', 'success');
    }
  }

  @override
  MainPageState rebuild(void Function(MainPageStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MainPageStateBuilder toBuilder() => new MainPageStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MainPageState &&
        error == other.error &&
        isLoading == other.isLoading &&
        isLogin == other.isLogin &&
        success == other.success;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, error.hashCode), isLoading.hashCode), isLogin.hashCode),
        success.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MainPageState')
          ..add('error', error)
          ..add('isLoading', isLoading)
          ..add('isLogin', isLogin)
          ..add('success', success))
        .toString();
  }
}

class MainPageStateBuilder
    implements Builder<MainPageState, MainPageStateBuilder> {
  _$MainPageState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _isLogin;
  bool get isLogin => _$this._isLogin;
  set isLogin(bool isLogin) => _$this._isLogin = isLogin;

  bool _success;
  bool get success => _$this._success;
  set success(bool success) => _$this._success = success;

  MainPageStateBuilder();

  MainPageStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _isLoading = _$v.isLoading;
      _isLogin = _$v.isLogin;
      _success = _$v.success;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MainPageState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MainPageState;
  }

  @override
  void update(void Function(MainPageStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MainPageState build() {
    final _$result = _$v ??
        new _$MainPageState._(
            error: error,
            isLoading: isLoading,
            isLogin: isLogin,
            success: success);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
