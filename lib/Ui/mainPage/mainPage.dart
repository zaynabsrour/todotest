import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
 import 'package:todotest/Ui/clientProfile/clientProfile.dart';
   import 'package:todotest/app/App.dart';
 import 'package:todotest/core/constent.dart';
import 'package:todotest/Ui/mainPage/bloc/main_page_bloc.dart';
import 'package:todotest/Ui/mainPage/bloc/main_page_event.dart';
import 'package:todotest/Ui/mainPage/bloc/main_page_state.dart';
import 'package:todotest/core/style/base_color.dart';
import 'package:todotest/core/style/dialogcustom.dart';
import 'package:todotest/Ui/homePage/home_page.dart';
import 'package:todotest/Ui/filterPage/page/filter_page.dart';
import '../../injectoin.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  final _bloc = sl<MainPageBloc>();
  int _currentIndex = 0;
  final List<Widget> _children = [];

  var _selectedPage = [
    HomePage(),
    FilterPage(),

    ClientProfile(),
  ];
bool isLogin;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc.add(GetIsLogin());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return BlocBuilder(
        cubit: _bloc,
        builder: (context, MainPageState state) {
          error(state.error);
          isLogin=state.isLogin;
          return WillPopScope(
            onWillPop: _onWillPop,
            child: Scaffold(
              key: scaffoldKey,
              appBar: AppBar(
                elevation: 0,
                backgroundColor: _currentIndex == 3
                    ? AppColor.background
                    : AppColor.greyColor,
                title: Text(
                  getTitle(_currentIndex),
                  style: TextStyle(
                    color: AppColor.yallowDark,
                  ),
                ),
                leading: InkWell(
                  onTap: () {
                    scaffoldKey.currentState.openDrawer();
                  },
                  child: Image.asset(
                    'assets/image/home_icon.png',
                    width: 30,
                    height: 30,
                    color: AppColor.yallowDark,
                  ),
                ),

              ),
              bottomNavigationBar: Container(
                height: size.height * 0.1,
                child: BottomNavigationBar(
                  onTap: (int index){
                    print("index:"+index.toString());
    //                 if(!state.isLogin){
    //   return showLogin(context);
    // }else
    setState(() {
      _currentIndex = index;
    });
                  },
                  currentIndex: _currentIndex,
//          backgroundColor: AppColors.appMainColor,
                  type: BottomNavigationBarType.fixed,
                  items: [
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.home,
                        color: Colors.grey,
                      ),
                      title: Text(

                          'Home',

                        style: TextStyle(
                          color: (_currentIndex == 0
                              ? AppColor.yallowDark
                              : AppColor.greyColor),
                        ),
//                style: TextStyle(color: AppColor.white),
                      ),
                      activeIcon: Icon(
                        Icons.home,
                        color: AppColor.yallowDark,
                      ),
                    ),
                    BottomNavigationBarItem(
                      icon: Image.asset(
                        'assets/image/rocket.png',
                        color: Colors.grey,
                      ),
                      title: Text(

                          'Filter',

                        style: TextStyle(
                          color: (_currentIndex == 1
                              ? AppColor.yallowDark
                              : AppColor.greyColor),
                        ),
//                style: TextStyle(color: AppColors.white),
                      ),
                      activeIcon: Image.asset(
                        'assets/image/rocket.png',
                        color: AppColor.yallowDark,
                      ),
                    ),


                    BottomNavigationBarItem(
                      icon: Icon(Icons.supervised_user_circle),
                      title: Text(

                          'Profile',

                        style: TextStyle(
                          color: (_currentIndex == 2
                              ? AppColor.yallowDark
                              : AppColor.greyColor),
                        ),
//                style: TextStyle(color: AppColors.white),
                      ),
                      activeIcon: Icon(Icons.supervised_user_circle),
                    ),
                  ],
                ),
              ),
              body: _selectedPage[_currentIndex],

            ),
          );
        });
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          // timeInSecForIosWeb: 1,
          backgroundColor: containerColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }



  String getTitle(int index) {
    switch (index) {
      case 0:
        return
          'Home'
        ;
      case 1:
        return
          'Filter'
        ;

      case 2:
        return
          'Profile';
    }
  }

  Future<bool> _onWillPop() async {
    if (_currentIndex == 0) {
      Navigator.pop(context);
      Navigator.of(context).pop(true);
      exit(0);
    }
    setState(() {
      _currentIndex = 0;
    });
  }
}
