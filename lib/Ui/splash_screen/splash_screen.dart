import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todotest/Ui/Auth/welcomePage/welcomePage.dart';
 import 'package:todotest/core/constent.dart';
import 'package:todotest/Ui/mainPage/mainPage.dart';
class SplashScreen extends StatefulWidget {
  final bool isLogin;
   SplashScreen(this.isLogin);
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Timer(Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => WelcomePage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColor.background,
      body: Container(
        width: size.width,
        height: size.height,
        child:
            Center(
              child: Text("Test Todo")/*Image.asset('assets/image/logo.png')*/,
            ),


      ),
    );
  }
}
