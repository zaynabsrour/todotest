 import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
 import 'package:todotest/Ui/splash_screen/splash_screen.dart';
 import '../injectoin.dart';
import 'bloc/app_bloc.dart';
import 'bloc/app_event.dart';
import 'bloc/app_state.dart';


class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final _AppBloc = sl<AppBloc>();

 String token;

  @override
  void initState() {

    _AppBloc.add(IniEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        cubit: _AppBloc,
        builder: (BuildContext context, AppState state) {
          return MaterialApp(
            title: "App",
            home: SplashScreen(state.loginState),//ProjectDetail(1),
            locale: Locale('en', ''),
            localizationsDelegates: [
              //AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: [
              const Locale('en', ''), // English
              const Locale('ar', ''), // Arabic
            ],
            theme: ThemeData(
              primarySwatch: Colors.green,
            ),
          );
        });
  }
}
