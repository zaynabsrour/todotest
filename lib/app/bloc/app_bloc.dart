import 'package:bloc/bloc.dart';
import 'package:todotest/data/repository/irepository.dart';
 import 'app_event.dart';
import 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  IRepository _repository;

  AppBloc(this._repository):super(AppState.initail());




  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is IniEvent) {
      final result = await _repository.getIsLogin();
      yield state.rebuild((b) => b..loginState = result);


     }

  }
}
