import 'package:flutter/material.dart';

const String BaseUrl = "https://new-backend.writables.ae/api/";

const String IS_LOGIN = "login";

const String APP_LANGUAGE = "App_language";

const String TOKEN = "token";

const String ID = "id";
const String NAME = "name";
const String EMAIL = "email";
const String IMAGE = "image";
const String NUMBER = "number";
const String MOBILE = "mobile";
 bool imageCheck(String imageUrl){
    if(imageUrl!=null&&imageUrl.contains("https://picsum.photos"))
      return true;
    else
      return false;  
 }

class AppColor {
  static const buttonColor = Color(0XFF3C3C3B);

  static const background2 = Color(0XFF3C3C3B);

  static const Color background = Color(0XFF3C3C3B);

  static const Color greenColor = Color(0xFFBCBCBC);

  static const Color lightGreenColor = Color(0xFFC2C2C2);

  static const Color textColor = Colors.black;

  static const Color yallowCollor = Color(0xFFfdd835);

  static const Color yallowDark = Color(0xFFE3AD08);

  static const containerColor = Color(0xFFf2f2f2);

  static const greyColor = Color(0xFFBCBCBC);

  static const textGreyColor = Color(0xFFC2C2C2);

  static const fillGreyColor = Color(0xFFF1F0F6);

  static const lightGreyColor = Color(0xFF888888);

  static const blueColor = Color(0xFF007EFC);

  static const lightGrey = Color(0xFFF1F0F6);

  static const redColor = Color(0xFFCF0000);

  static const lightBlack = Color(0xFF757575);
}
