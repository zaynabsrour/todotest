import 'package:flutter/material.dart';

const blueColor = Color(0xFF3076D8);
const cyanColor = "#1baad4";

const containerColor = Colors.grey;

const textColor = Colors.black38;

const darkBlue = Color(0xFF3076D8);
const lightBlue = Color.fromARGB(25, 0, 181, 255);
const yalowColor = Color(0xFFFFA814);
const baseColor = Color(0xFFE3AD08);
const greyColor = Color(0xffE0E0E0);
const textFieldColor = Color(0xFFEEEEEE);
const greenLight = Color(0xff3CA47E);

const greyTextColor = Color(0xffBCBCBC);
const darkGreyTextColor = Color(0xff8B8B8B);

const lightGrey = Color(0xffF1F0F6);

const iconsColorGrey = Color(0xffC2C2C2);
const borderColor = Color(0xffF1F0F6);
const redColor = Color(0xffE40000);


const offBlack = Color(0XFF3C3C3B);

const checkBoxColor = Color(0XFFF9A825);


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.tryParse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

