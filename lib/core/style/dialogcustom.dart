import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';

 import 'package:todotest/Ui/Auth/welcomePage/welcomePage.dart';



showLogin(var context){
  return  AwesomeDialog(
    context: context,
    customHeader: Container(
      child: Icon(
        Icons.error_outline,
        size: 100,
        color: Colors.yellow,
      ),
    ),
    btnCancelText:  "Cancel",
    btnOkText:  "Ok",
    btnOkColor:Colors.yellow,
    dialogType: DialogType.INFO,
    animType: AnimType.BOTTOMSLIDE,
    title:
    "Login",

    desc:
     "You must be logged in",

    btnCancelOnPress: () {},
    btnOkOnPress: () {
      WidgetsBinding.instance.addPostFrameCallback((_) =>
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => WelcomePage())));
    },
  )..show();
}


