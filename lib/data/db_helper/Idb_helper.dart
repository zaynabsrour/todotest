

 import 'package:todotest/data/db_helper/entite/todo.dart';

import 'db/database.dart';

abstract class IDbHelper {
  Future<AppDatabase> _getInstDB() {}


  //////////////////

  Future<void> insertTodo(Todo todo);

  Future<List<Todo>> getTodo();


  Future<void> deleteTodo(int id);
  Future<void> updateTodo(Todo todo);
  Future<List<Todo>> getTodoStatus(String status);





}