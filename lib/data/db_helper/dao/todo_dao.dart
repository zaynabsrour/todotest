import 'package:floor/floor.dart';
import 'package:todotest/data/db_helper/entite/todo.dart';

@dao
abstract class TodoDao {

  @insert
  Future<void> insertTodo(Todo todo);

  @Query('SELECT * FROM Todo ')
  Future<List<Todo>> getTodo();

  @Query('DELETE FROM Todo where id = :id')
  Future<void> deleteTodo(int id);
  @update
  Future<void> updateTodo(Todo todo);


  @Query('SELECT * FROM Todo where status=:status ')
  Future<List<Todo>> getTodoStatus(String status);



}
