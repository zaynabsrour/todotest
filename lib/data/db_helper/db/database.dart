import 'dart:async';
import 'package:floor/floor.dart';
 import 'package:todotest/data/db_helper/entite/todo.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
 import 'package:todotest/data/db_helper/dao/todo_dao.dart';

part 'database.g.dart'; // the generated code will be there

@Database(
    version: 1, entities: [Todo])
abstract class AppDatabase extends FloorDatabase {


  TodoDao get todoDao;



}

