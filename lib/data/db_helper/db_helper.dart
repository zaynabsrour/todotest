

 import 'package:todotest/data/db_helper/entite/todo.dart';

import 'Idb_helper.dart';
import 'db/database.dart';

class DbHelper implements IDbHelper {
  @override
  Future<AppDatabase> _getInstDB() async {
    return await $FloorAppDatabase.databaseBuilder('idecoDB.db').build();
  }




  /////////////////////////////////

  @override
  Future<void> insertTodo(Todo todo) async{
    try {
      return await (await _getInstDB()).todoDao.insertTodo(todo);
    } catch (e) {
      print('todoList db e is $e');
      throw Exception(e.toString());
    }
  }


  @override
  Future<List<Todo>> getTodo()async {
    try {
      return await (await _getInstDB()).todoDao.getTodo();
    } catch (e) {
      print('todoList db e is $e');
      throw Exception(e.toString());
    }
  }


  @override
  Future<void> deleteTodo(int id) async{
    try {
      return await (await _getInstDB()).todoDao.deleteTodo(id);
    } catch (e) {
      print('todoList db e is $e');
      throw Exception(e.toString());
    }
  }
  @override
  Future<void> updateTodo(Todo todo) async{
    try {
      return await (await _getInstDB()).todoDao.updateTodo(todo);
    } catch (e) {
      print('todoList db e is $e');
      throw Exception(e.toString());
    }
  }
  @override
  Future<List<Todo>> getTodoStatus(String status)async {
    try {
      return await (await _getInstDB()).todoDao.getTodoStatus(status);
    } catch (e) {
      print('todoList db e is $e');
      throw Exception(e.toString());
    }
  }


}