import 'package:floor/floor.dart';

@entity
class Todo {
  @PrimaryKey(autoGenerate: true)
  int id;
  String title ;
  String description;
  String status;
  String date;


  Todo( {this.id, this.title , this.description,this.status,this.date});
}
