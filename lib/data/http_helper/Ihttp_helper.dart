
import 'dart:io';


import 'package:built_collection/built_collection.dart';
import 'package:todotest/model/login_model/login_model.dart';
import 'package:todotest/model/user/user.dart';
 import 'package:todotest/model/user_response/user_response.dart';


abstract class IHttpHelper {
  Future<LoginModel> login( String email, String password);
  Future<LoginModel> refreshToken( String refreshToken);
  Future<User>getUserProfile(String token);






}
