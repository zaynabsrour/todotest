import 'dart:convert';
import 'dart:io';
 import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';
 import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:todotest/model/login_model/login_model.dart';

import 'Ihttp_helper.dart';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:built_collection/built_collection.dart';
import 'package:todotest/model/user_response/user_response.dart';
import 'package:todotest/model/user/user.dart';

import 'package:todotest/model/serializer/serializer.dart';
import 'package:todotest/core/error/error.dart';
import 'package:path/path.dart';

  class HttpHelper implements IHttpHelper {
  final Dio _dio;

  var cookieJar = CookieJar();

  HttpHelper(this._dio) {
    _dio.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
      ),
    );
    _dio.interceptors.add(CookieManager(cookieJar));
  }

  @override
  Future<LoginModel> login( String email, String password) async {
    try {

      Map formData={
        "password": password,
        "email": email,

      };

      final response = await _dio.post('public/auth/signIn',
          data: formData);
      print('Login Response StatusCode ${response.statusCode}');

      if (response.statusCode == 200) {
        final userResponse = serializers.deserialize(
            json.decode(response.data)['response']['data'],
            specifiedType: FullType(LoginModel));
        print("implement Login status : ${userResponse}");
        if (userResponse != null) {
          return userResponse;
        } else {
          throw NetworkException();
        }
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e.toString());
      throw NetworkException();
    }
  }
  @override
  Future<LoginModel> refreshToken( String refreshToken) async {
    try {

      Map formData={
        "refreshToken": refreshToken,


      };

      final response = await _dio.post('public/auth/refresh-token',
          data: formData);
      print('refresh token Response StatusCode ${response.statusCode}');

      if (response.statusCode == 200) {
        final userResponse = serializers.deserialize(
            json.decode(response.data)['response']['data'],
            specifiedType: FullType(LoginModel));
        print("implement refresh token status : ${userResponse}");
        if (userResponse != null) {
          return userResponse;
        } else {
          throw NetworkException();
        }
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e.toString());
      throw NetworkException();
    }
  }




  @override
    Future<User>getUserProfile(String token)async{
try {
      final response = await _dio.get(
        "private/user",
        options: Options(headers: {"Authorization": "Bearer $token"})
      );
      print('get User Profile Response StatusCode ${response.statusCode}');

      if (response.statusCode == 200) {
        final  user = serializers.deserialize(
            json.decode(response.data)['response']['data'],
            specifiedType: FullType(User));
        if (user != null) {
          return user;
        } else {
          throw NetworkException();
        }
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e.toString());
      throw NetworkException();
    }
    }




}
