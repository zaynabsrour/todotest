

import 'package:shared_preferences/shared_preferences.dart';
import 'package:todotest/model/user/user.dart';

abstract class IPrefsHelper {
  Future<void> saveToken(String token, bool active);

  Future<String> getToken();


  Future<void> setAppLanguage(int value);
  Future<int> getUserId();
  Future<bool> getIsLogin();
  Future<void> setIsLogin();
  Future<bool> logout();
}
