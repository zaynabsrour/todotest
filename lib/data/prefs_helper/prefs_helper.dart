

import 'package:shared_preferences/shared_preferences.dart';
import 'package:todotest/core/constent.dart';
import 'package:todotest/model/user/user.dart';
 import 'iprefs_helper.dart';

class PrefsHelper implements IPrefsHelper {
  @override

  @override
  Future<int> getUserId() async {
    return (await getPrefs()).getInt(ID) ?? 0;
  }
  @override
  Future<bool> logout() async{
    // TODO: implement logout
    (await getPrefs()).setBool(IS_LOGIN, false);
  }
  @override
  Future<void> setIsLogin() async{
    // TODO: implement setIsLogin
    (await getPrefs()).setBool(IS_LOGIN, true);
  }
  @override
  Future<bool> getIsLogin() async {
    // TODO: implement getIsLogin
    return (await getPrefs()).getBool(IS_LOGIN) ?? false;
  }

  @override
  Future<void> setAppLanguage(int value) async{
    (await getPrefs()).setInt(APP_LANGUAGE,value);
  }
  @override
  Future<SharedPreferences> getPrefs() async {
    return await SharedPreferences.getInstance();
  }
 @override
    Future<String> getToken() async {
      return ((await getPrefs()).getString(TOKEN));
    }
      @override
    Future<void> saveToken(String token, bool active)async {
      (await getPrefs()).setString(TOKEN, token);
      if(active)
      (await getPrefs()).setBool(IS_LOGIN, true);
    }

  




}
