import 'dart:io';
 import 'package:built_collection/built_collection.dart';
import 'package:todotest/model/login_model/login_model.dart';
import 'package:todotest/data/db_helper/entite/todo.dart';
import 'package:todotest/model/user/user.dart';



abstract class IRepository {
  Future<bool> getIsLogin();
   Future<bool> logout();
  Future<LoginModel> login( String email, String password);
  Future<LoginModel> refreshToken( String refreshToken);
  Future<User>getUserProfile();
  Future<void> insertTodo(Todo todo);
  Future<List<Todo>> getTodo();
  Future<void> deleteTodo(int id);
  Future<void> updateTodo(Todo todo);
  Future<List<Todo>> getTodoStatus(String status);




}
