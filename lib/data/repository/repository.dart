import 'dart:ffi';
import 'dart:io';
import 'dart:io';

import 'package:built_collection/src/list.dart';

import 'package:todotest/data/http_helper/Ihttp_helper.dart';
import 'package:todotest/data/prefs_helper/iprefs_helper.dart';
import 'package:todotest/model/login_model/login_model.dart';
import 'package:todotest/data/db_helper/entite/todo.dart';
import 'package:todotest/model/user/user.dart';
 import 'package:todotest/model/user_response/user_response.dart';
import 'irepository.dart';
import 'package:todotest/data/db_helper/Idb_helper.dart';

class Repository implements IRepository {
  IHttpHelper _ihttpHelper;
  IPrefsHelper _iprefHelper;

  IDbHelper _iDbHelper;

  Repository(this._ihttpHelper, this._iprefHelper,this._iDbHelper);

  @override
  Future<bool> getIsLogin() async {
    // TODO: implement getIsLogin
    return await _iprefHelper.getIsLogin();
  }

  @override
  Future<bool> logout() async {
    // TODO: implement logout
    final appLogout = await _iprefHelper.logout();
    return appLogout;
  }
  @override
  Future<LoginModel> login( String email, String password)async{
    final login=await _ihttpHelper.login(email, password);
    final save=await _iprefHelper.saveToken(login.accessToken, true);
    return login;

  }
  @override
  Future<LoginModel> refreshToken( String refreshToken)async{
    final refresh=await _ihttpHelper.refreshToken(refreshToken);
    final save=await _iprefHelper.saveToken(refresh.accessToken, true);
    return refresh;

  }
  @override
  Future<User>getUserProfile()async{
    final token=await _iprefHelper.getToken();
    final user=await _ihttpHelper.getUserProfile(token);

    return user;

  }
  ////////////////////////////////////////////////////////////////////
  @override
  Future<void> insertTodo(Todo todo) async {
    try {
      final List<Todo> result = await _iDbHelper.getTodo();
      if (result == null || result.isEmpty) {
        _iDbHelper.insertTodo(todo);
      } else {
        for (var item in result) {
          if (item.id == todo.id) {
            print('Exception Exception');
            throw Exception("");
          }
        }
        _iDbHelper.insertTodo(todo);
      }
    } catch (e) {
      throw Exception("");
    }
  }
  @override
  Future<List<Todo>> getTodo() {
    return _iDbHelper.getTodo();
  }

  @override
  Future<void> deleteTodo(int id) {
    return _iDbHelper.deleteTodo(id);
  }
  @override
  Future<void> updateTodo(Todo todo) {
    return _iDbHelper.updateTodo(todo);
  }
  @override
  Future<List<Todo>> getTodoStatus(String status) {
    return _iDbHelper.getTodoStatus(status);
  }





}
