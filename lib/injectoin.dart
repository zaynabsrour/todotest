import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
 import 'app/bloc/app_bloc.dart';
import 'core/constent.dart';
import 'package:todotest/Ui/homePage/bloc/home_page_bloc.dart';
import 'package:todotest/Ui/filterPage/bloc/filter_page_bloc.dart';
//import 'data/db_helper/Idb_helper.dart';
//import 'data/db_helper/db_helper.dart';
import 'data/http_helper/Ihttp_helper.dart';
import 'data/http_helper/http_helper.dart';
import 'data/prefs_helper/iprefs_helper.dart';
import 'data/prefs_helper/prefs_helper.dart';
import 'data/repository/irepository.dart';
import 'data/repository/repository.dart';
import 'package:todotest/Ui/Auth/bloc/auth_bloc.dart';
import 'package:todotest/Ui/clientProfile/bloc/client_profile_bloc.dart';
 import 'package:todotest/Ui/mainPage/bloc/main_page_bloc.dart';
 import 'package:todotest/data/db_helper/Idb_helper.dart';
 import 'package:todotest/data/db_helper/db_helper.dart';
final sl = GetIt.instance;

Future iniGetIt() async {
  sl.registerLazySingleton(() => ((Dio(BaseOptions(
      baseUrl: BaseUrl,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "charset": "utf-8",
        "Accept-Charset": "utf-8"
      },
      responseType: ResponseType.plain)))));

  // data
  sl.registerLazySingleton<IDbHelper>(() => DbHelper());
  sl.registerLazySingleton<IPrefsHelper>(() => PrefsHelper());
  sl.registerLazySingleton<IHttpHelper>(() => HttpHelper(sl()));
  sl.registerLazySingleton<IRepository>(() => Repository(sl(), sl(),sl()));

  /// AppBloc

  sl.registerFactory(() => AppBloc(sl()));
  sl.registerFactory(() => AuthBloc(sl()));
  sl.registerFactory(() => ClientProfileBloc(sl()));

   sl.registerFactory(() => MainPageBloc(sl()));
  sl.registerFactory(() => HomePageBloc(sl()));
  sl.registerFactory(() => FilterPageBloc(sl()));




}
