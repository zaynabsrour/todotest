library address;

import 'dart:convert';
import 'dart:ffi';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:todotest/model/serializer/serializer.dart';

import 'package:built_collection/built_collection.dart';
 part 'address.g.dart';

abstract class Address implements Built<Address, AddressBuilder> {
  String get id;


  @nullable
  String get name;
  @nullable
  String get phoneCode;
  @nullable
  double get latitude;
  @nullable
  double get longitude;





  Address._();

  factory Address([updates(AddressBuilder b)]) = _$Address;

  String toJson() {
    return json.encode(serializers.serializeWith(Address.serializer, this));
  }

  static Address fromJson(String jsonString) {
    return serializers.deserializeWith(
        Address.serializer, json.decode(jsonString));
  }

  static Serializer<Address> get serializer => _$addressSerializer;
}
