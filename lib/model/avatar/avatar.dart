library avatar;

import 'dart:convert';
import 'dart:ffi';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:todotest/model/serializer/serializer.dart';

import 'package:built_collection/built_collection.dart';
 part 'avatar.g.dart';

abstract class Avatar implements Built<Avatar, AvatarBuilder> {
  String get id;

  @nullable
  bool get deleted;
  @nullable
  String get name;
  @nullable
  String get uri;
  @nullable
  int get size;
  @nullable
   String get mimetype;





  Avatar._();

  factory Avatar([updates(AvatarBuilder b)]) = _$Avatar;

  String toJson() {
    return json.encode(serializers.serializeWith(Avatar.serializer, this));
  }

  static Avatar fromJson(String jsonString) {
    return serializers.deserializeWith(
        Avatar.serializer, json.decode(jsonString));
  }

  static Serializer<Avatar> get serializer => _$avatarSerializer;
}
