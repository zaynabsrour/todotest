// GENERATED CODE - DO NOT MODIFY BY HAND

part of avatar;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Avatar> _$avatarSerializer = new _$AvatarSerializer();

class _$AvatarSerializer implements StructuredSerializer<Avatar> {
  @override
  final Iterable<Type> types = const [Avatar, _$Avatar];
  @override
  final String wireName = 'Avatar';

  @override
  Iterable<Object> serialize(Serializers serializers, Avatar object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];
    if (object.deleted != null) {
      result
        ..add('deleted')
        ..add(serializers.serialize(object.deleted,
            specifiedType: const FullType(bool)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.uri != null) {
      result
        ..add('uri')
        ..add(serializers.serialize(object.uri,
            specifiedType: const FullType(String)));
    }
    if (object.size != null) {
      result
        ..add('size')
        ..add(serializers.serialize(object.size,
            specifiedType: const FullType(int)));
    }
    if (object.mimetype != null) {
      result
        ..add('mimetype')
        ..add(serializers.serialize(object.mimetype,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Avatar deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AvatarBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'deleted':
          result.deleted = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'uri':
          result.uri = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'size':
          result.size = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'mimetype':
          result.mimetype = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Avatar extends Avatar {
  @override
  final String id;
  @override
  final bool deleted;
  @override
  final String name;
  @override
  final String uri;
  @override
  final int size;
  @override
  final String mimetype;

  factory _$Avatar([void Function(AvatarBuilder) updates]) =>
      (new AvatarBuilder()..update(updates)).build();

  _$Avatar._(
      {this.id, this.deleted, this.name, this.uri, this.size, this.mimetype})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('Avatar', 'id');
    }
  }

  @override
  Avatar rebuild(void Function(AvatarBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AvatarBuilder toBuilder() => new AvatarBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Avatar &&
        id == other.id &&
        deleted == other.deleted &&
        name == other.name &&
        uri == other.uri &&
        size == other.size &&
        mimetype == other.mimetype;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, id.hashCode), deleted.hashCode), name.hashCode),
                uri.hashCode),
            size.hashCode),
        mimetype.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Avatar')
          ..add('id', id)
          ..add('deleted', deleted)
          ..add('name', name)
          ..add('uri', uri)
          ..add('size', size)
          ..add('mimetype', mimetype))
        .toString();
  }
}

class AvatarBuilder implements Builder<Avatar, AvatarBuilder> {
  _$Avatar _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  bool _deleted;
  bool get deleted => _$this._deleted;
  set deleted(bool deleted) => _$this._deleted = deleted;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _uri;
  String get uri => _$this._uri;
  set uri(String uri) => _$this._uri = uri;

  int _size;
  int get size => _$this._size;
  set size(int size) => _$this._size = size;

  String _mimetype;
  String get mimetype => _$this._mimetype;
  set mimetype(String mimetype) => _$this._mimetype = mimetype;

  AvatarBuilder();

  AvatarBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _deleted = _$v.deleted;
      _name = _$v.name;
      _uri = _$v.uri;
      _size = _$v.size;
      _mimetype = _$v.mimetype;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Avatar other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Avatar;
  }

  @override
  void update(void Function(AvatarBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Avatar build() {
    final _$result = _$v ??
        new _$Avatar._(
            id: id,
            deleted: deleted,
            name: name,
            uri: uri,
            size: size,
            mimetype: mimetype);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
