library login_model;

import 'dart:convert';
import 'dart:ffi';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:todotest/model/serializer/serializer.dart';



import 'package:built_collection/built_collection.dart';

part 'login_model.g.dart';

abstract class LoginModel implements Built<LoginModel, LoginModelBuilder> {

  @nullable
  String get accessToken;
  @nullable
  String get refreshToken;



  LoginModel._();

  factory LoginModel([updates(LoginModelBuilder b)]) = _$LoginModel;

  String toJson() {
    return json.encode(serializers.serializeWith(LoginModel.serializer, this));
  }

  static LoginModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        LoginModel.serializer, json.decode(jsonString));
  }

  static Serializer<LoginModel> get serializer => _$loginModelSerializer;
}
