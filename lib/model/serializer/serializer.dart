library serializer;
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_collection/built_collection.dart';
import 'package:todotest/model/address/address.dart';
import 'package:todotest/model/avatar/avatar.dart';
import 'package:todotest/model/login_model/login_model.dart';


import 'package:todotest/model/user/user.dart';

import 'package:todotest/model/user_response/user_response.dart';

part 'serializer.g.dart';

@SerializersFor(const [
User,

  LoginModel,
  Avatar,
  Address

])
final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin())

      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(User),
            ],
          )),
          () => ListBuilder<User>())
  ..addBuilderFactory(
      (FullType(
        BuiltList,
        [
          const FullType(Avatar),
        ],
      )),
          () => ListBuilder<Avatar>())
  ..addBuilderFactory(
      (FullType(
        BuiltList,
        [
          const FullType(Address),
        ],
      )),
          () => ListBuilder<Address>())




          ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(UserResponse),
            ],
          )),
          () => ListBuilder<UserResponse>())





          ..addBuilderFactory(
      (FullType(
        BuiltList,
        [
          const FullType(String),
        ],
      )),
          () => ListBuilder<String>())








)
    .build();
