library user;

import 'dart:convert';
import 'dart:ffi';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:todotest/model/address/address.dart';
import 'package:todotest/model/avatar/avatar.dart';
import 'package:todotest/model/serializer/serializer.dart';

import 'package:built_collection/built_collection.dart';
 part 'user.g.dart';

abstract class User implements Built<User, UserBuilder> {
  String get id;
  @nullable
  bool get active;
  @nullable
  bool get verified;
  @nullable
  bool get deleted;
  @nullable
  String get name;
  @nullable
  String get type;
  @nullable
  String get email;
  @nullable
   String get phone;
  @nullable
  String get promoCode;
  @nullable
  String get aboutMe;
  @nullable
  String get title;

  @nullable
   String get birthday;
  @nullable
   String get createdAt;
  @nullable
  String get verifyCode;
  @nullable
  Avatar get avatar;
  @nullable
  Avatar get idPicture;
  @nullable
  Address get address;




  User._();

  factory User([updates(UserBuilder b)]) = _$User;

  String toJson() {
    return json.encode(serializers.serializeWith(User.serializer, this));
  }

  static User fromJson(String jsonString) {
    return serializers.deserializeWith(
        User.serializer, json.decode(jsonString));
  }

  static Serializer<User> get serializer => _$userSerializer;
}
