// GENERATED CODE - DO NOT MODIFY BY HAND

part of user;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<User> _$userSerializer = new _$UserSerializer();

class _$UserSerializer implements StructuredSerializer<User> {
  @override
  final Iterable<Type> types = const [User, _$User];
  @override
  final String wireName = 'User';

  @override
  Iterable<Object> serialize(Serializers serializers, User object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];
    if (object.active != null) {
      result
        ..add('active')
        ..add(serializers.serialize(object.active,
            specifiedType: const FullType(bool)));
    }
    if (object.verified != null) {
      result
        ..add('verified')
        ..add(serializers.serialize(object.verified,
            specifiedType: const FullType(bool)));
    }
    if (object.deleted != null) {
      result
        ..add('deleted')
        ..add(serializers.serialize(object.deleted,
            specifiedType: const FullType(bool)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.phone != null) {
      result
        ..add('phone')
        ..add(serializers.serialize(object.phone,
            specifiedType: const FullType(String)));
    }
    if (object.promoCode != null) {
      result
        ..add('promoCode')
        ..add(serializers.serialize(object.promoCode,
            specifiedType: const FullType(String)));
    }
    if (object.aboutMe != null) {
      result
        ..add('aboutMe')
        ..add(serializers.serialize(object.aboutMe,
            specifiedType: const FullType(String)));
    }
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    if (object.birthday != null) {
      result
        ..add('birthday')
        ..add(serializers.serialize(object.birthday,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add('createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(String)));
    }
    if (object.verifyCode != null) {
      result
        ..add('verifyCode')
        ..add(serializers.serialize(object.verifyCode,
            specifiedType: const FullType(String)));
    }
    if (object.avatar != null) {
      result
        ..add('avatar')
        ..add(serializers.serialize(object.avatar,
            specifiedType: const FullType(Avatar)));
    }
    if (object.idPicture != null) {
      result
        ..add('idPicture')
        ..add(serializers.serialize(object.idPicture,
            specifiedType: const FullType(Avatar)));
    }
    if (object.address != null) {
      result
        ..add('address')
        ..add(serializers.serialize(object.address,
            specifiedType: const FullType(Address)));
    }
    return result;
  }

  @override
  User deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'active':
          result.active = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'verified':
          result.verified = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'deleted':
          result.deleted = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'phone':
          result.phone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'promoCode':
          result.promoCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'aboutMe':
          result.aboutMe = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'birthday':
          result.birthday = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'verifyCode':
          result.verifyCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'avatar':
          result.avatar.replace(serializers.deserialize(value,
              specifiedType: const FullType(Avatar)) as Avatar);
          break;
        case 'idPicture':
          result.idPicture.replace(serializers.deserialize(value,
              specifiedType: const FullType(Avatar)) as Avatar);
          break;
        case 'address':
          result.address.replace(serializers.deserialize(value,
              specifiedType: const FullType(Address)) as Address);
          break;
      }
    }

    return result.build();
  }
}

class _$User extends User {
  @override
  final String id;
  @override
  final bool active;
  @override
  final bool verified;
  @override
  final bool deleted;
  @override
  final String name;
  @override
  final String type;
  @override
  final String email;
  @override
  final String phone;
  @override
  final String promoCode;
  @override
  final String aboutMe;
  @override
  final String title;
  @override
  final String birthday;
  @override
  final String createdAt;
  @override
  final String verifyCode;
  @override
  final Avatar avatar;
  @override
  final Avatar idPicture;
  @override
  final Address address;

  factory _$User([void Function(UserBuilder) updates]) =>
      (new UserBuilder()..update(updates)).build();

  _$User._(
      {this.id,
      this.active,
      this.verified,
      this.deleted,
      this.name,
      this.type,
      this.email,
      this.phone,
      this.promoCode,
      this.aboutMe,
      this.title,
      this.birthday,
      this.createdAt,
      this.verifyCode,
      this.avatar,
      this.idPicture,
      this.address})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('User', 'id');
    }
  }

  @override
  User rebuild(void Function(UserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserBuilder toBuilder() => new UserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is User &&
        id == other.id &&
        active == other.active &&
        verified == other.verified &&
        deleted == other.deleted &&
        name == other.name &&
        type == other.type &&
        email == other.email &&
        phone == other.phone &&
        promoCode == other.promoCode &&
        aboutMe == other.aboutMe &&
        title == other.title &&
        birthday == other.birthday &&
        createdAt == other.createdAt &&
        verifyCode == other.verifyCode &&
        avatar == other.avatar &&
        idPicture == other.idPicture &&
        address == other.address;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        0,
                                                                        id
                                                                            .hashCode),
                                                                    active
                                                                        .hashCode),
                                                                verified
                                                                    .hashCode),
                                                            deleted.hashCode),
                                                        name.hashCode),
                                                    type.hashCode),
                                                email.hashCode),
                                            phone.hashCode),
                                        promoCode.hashCode),
                                    aboutMe.hashCode),
                                title.hashCode),
                            birthday.hashCode),
                        createdAt.hashCode),
                    verifyCode.hashCode),
                avatar.hashCode),
            idPicture.hashCode),
        address.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('User')
          ..add('id', id)
          ..add('active', active)
          ..add('verified', verified)
          ..add('deleted', deleted)
          ..add('name', name)
          ..add('type', type)
          ..add('email', email)
          ..add('phone', phone)
          ..add('promoCode', promoCode)
          ..add('aboutMe', aboutMe)
          ..add('title', title)
          ..add('birthday', birthday)
          ..add('createdAt', createdAt)
          ..add('verifyCode', verifyCode)
          ..add('avatar', avatar)
          ..add('idPicture', idPicture)
          ..add('address', address))
        .toString();
  }
}

class UserBuilder implements Builder<User, UserBuilder> {
  _$User _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  bool _active;
  bool get active => _$this._active;
  set active(bool active) => _$this._active = active;

  bool _verified;
  bool get verified => _$this._verified;
  set verified(bool verified) => _$this._verified = verified;

  bool _deleted;
  bool get deleted => _$this._deleted;
  set deleted(bool deleted) => _$this._deleted = deleted;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _phone;
  String get phone => _$this._phone;
  set phone(String phone) => _$this._phone = phone;

  String _promoCode;
  String get promoCode => _$this._promoCode;
  set promoCode(String promoCode) => _$this._promoCode = promoCode;

  String _aboutMe;
  String get aboutMe => _$this._aboutMe;
  set aboutMe(String aboutMe) => _$this._aboutMe = aboutMe;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _birthday;
  String get birthday => _$this._birthday;
  set birthday(String birthday) => _$this._birthday = birthday;

  String _createdAt;
  String get createdAt => _$this._createdAt;
  set createdAt(String createdAt) => _$this._createdAt = createdAt;

  String _verifyCode;
  String get verifyCode => _$this._verifyCode;
  set verifyCode(String verifyCode) => _$this._verifyCode = verifyCode;

  AvatarBuilder _avatar;
  AvatarBuilder get avatar => _$this._avatar ??= new AvatarBuilder();
  set avatar(AvatarBuilder avatar) => _$this._avatar = avatar;

  AvatarBuilder _idPicture;
  AvatarBuilder get idPicture => _$this._idPicture ??= new AvatarBuilder();
  set idPicture(AvatarBuilder idPicture) => _$this._idPicture = idPicture;

  AddressBuilder _address;
  AddressBuilder get address => _$this._address ??= new AddressBuilder();
  set address(AddressBuilder address) => _$this._address = address;

  UserBuilder();

  UserBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _active = _$v.active;
      _verified = _$v.verified;
      _deleted = _$v.deleted;
      _name = _$v.name;
      _type = _$v.type;
      _email = _$v.email;
      _phone = _$v.phone;
      _promoCode = _$v.promoCode;
      _aboutMe = _$v.aboutMe;
      _title = _$v.title;
      _birthday = _$v.birthday;
      _createdAt = _$v.createdAt;
      _verifyCode = _$v.verifyCode;
      _avatar = _$v.avatar?.toBuilder();
      _idPicture = _$v.idPicture?.toBuilder();
      _address = _$v.address?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(User other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$User;
  }

  @override
  void update(void Function(UserBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$User build() {
    _$User _$result;
    try {
      _$result = _$v ??
          new _$User._(
              id: id,
              active: active,
              verified: verified,
              deleted: deleted,
              name: name,
              type: type,
              email: email,
              phone: phone,
              promoCode: promoCode,
              aboutMe: aboutMe,
              title: title,
              birthday: birthday,
              createdAt: createdAt,
              verifyCode: verifyCode,
              avatar: _avatar?.build(),
              idPicture: _idPicture?.build(),
              address: _address?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'avatar';
        _avatar?.build();
        _$failedField = 'idPicture';
        _idPicture?.build();
        _$failedField = 'address';
        _address?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'User', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
