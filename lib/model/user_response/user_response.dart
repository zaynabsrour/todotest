library user_response;

import 'dart:convert';
import 'dart:ffi';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:todotest/model/serializer/serializer.dart';
import 'package:todotest/model/user/user.dart';
import 'package:built_collection/built_collection.dart';

part 'user_response.g.dart';

abstract class UserResponse implements Built<UserResponse, UserResponseBuilder> {
  @nullable
  String get token;
  @nullable
  User get user;
  
  UserResponse._();

  factory UserResponse([updates(UserResponseBuilder b)]) = _$UserResponse;

  String toJson() {
    return json.encode(serializers.serializeWith(UserResponse.serializer, this));
  }

  static UserResponse fromJson(String jsonString) {
    return serializers.deserializeWith(
        UserResponse.serializer, json.decode(jsonString));
  }

  static Serializer<UserResponse> get serializer => _$userResponseSerializer;
}
